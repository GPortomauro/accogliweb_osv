<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php $view['slots']->output('title', 'PROGETTO online') ?></title>
	    <?php $view['slots']->output('stylesheets') ?>
        <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
	    <link href="<?php echo $view['assets']->getUrl('css/main.css') ?>" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="sidebar">
            <?php if ($view['slots']->has('sidebar')): ?>
                <?php $view['slots']->output('sidebar') ?>
            <?php else: ?>
                <ul>
                    <li><a href="<?php echo $view['router']->generate('_welcome') ?>">Home</a></li>
                </ul>
            <?php endif; ?>
        </div>

        <div id="content">
            <img src="<?php echo $view['assets']->getUrl('images/symfony.gif') ?>" alt="Symfony!" />
            <?php $view['slots']->output('_content') ?>
        </div>
    </body>
</html>


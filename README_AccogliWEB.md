# ACCOGLIweb

Gestore del flusso clienti per strutture ricettive (hotel, residence, B&B, camping, ...)

ACCOGLIweb, web application sviluppata per mezzo del framework Symfony2, può essere impiegata su qualsiasi  dispositivo, con qualsiasi Sistema Operativo (Windows, Linux, Mac, Android, ...), in grado di utilizzare un browser (IE, Firefox, Chrome, Safari, Opera, ...)

Il codice sorgente di ACCOGLIweb è Open Source distribuito con licenza GNU General Public License, Ver. 3
disponibile su repository GitLab.

### Guida all'installazione su Sistema Operativo Linux

##### Servizi e relativi pacchetti di installazione
**Installazione di Php**
<br>
**(Distribuzione: Ubuntu 16.04 LTS)**
```sh
$ sudo apt-get install php-common php7.0-common php7.0-json php7.0-opcache \
  php7.0-readline php7.0-cli php7.0-xml php7.0-intl php7.0-mysql php-apcu
```
**(Distribuzione: Ubuntu 18.04 LTS)**
```sh
$ sudo apt-get install php php7.2-xml php7.2-intl php7.2-mysql php-apcu
```
abilitare il modulo APCu
```sh
$ sudo phpenmod apcu
```
ed in coda al file /etc/php/7.0/mods-available/apcu.ini aggiungere:
```sh
  apc.enable=1
  apc.enable_cli=1
```
**Installazione di Apache2**
<br>
**(Distribuzione: Ubuntu 16.04 LTS)**
```sh
$ sudo apt-get install apache2 apache2-bin apache2-data apache2-utils libapr1 \
  libaprutil1 libaprutil1-dbd-sqlite3 libaprutil1-ldap ssl-cert \
  liblua5.1-0 libapache2-mod-php7.0
```
**(Distribuzione: Ubuntu 18.04 LTS)**
```sh
$ sudo apt-get install apache2 liblua5.1-0 libapache2-mod-php7.0 ssl-cert
```
**Installazione di MySQL (o MariaDB)**
```sh
$ sudo apt-get install mysql-server
```
**Installazione di altri pacchetti di supporto**
```sh
$ sudo apt-get install acl zip unzip openssl curl git
```
**Installazione di Composer (tramite curl)**
```sh
$ curl -sS https://getcomposer.org/installer | php
$ sudo mv composer.phar /usr/local/bin/composer
```

**Interventi sul DB**

Con il client di MySQL creare il db "storesymfony"
```sh
$ mysql -u root -p
mysql> CREATE DATABASE storesymfony;
mysql> show databases;
mysql> quit
```

**Intallazione di Symfony 2 (ver. 2.8.\*) e dei file del progetto**

Creare la directory denominata "accogliweb" in /var/www:
```sh
$ cd /var/www
$ sudo mkdir accogliweb
```
Clonare localmente il progetto tramite Git e renderlo accessibile ad Apache:
```sh
$ sudo git clone https://gitlab.com/GPortomauro/accogliweb_osv.git accogliweb
$ sudo chown -R www-data:www-data accogliweb
```
Installare pacchetti e dipendenze tramite Composer (come ultimo passaggio Composer configurerà i parametri per l'accesso al db, inserire il nome del db creato nei passi precedenti):
```sh
$ cd accogliweb
$ sudo -u www-data composer update
```
Rendere accessibili a www-data le directory app/cache e app/logs:
```sh
$ HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
$ sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
$ sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
```
Verifico l'installazione ed i componenti a disposizione di Symfony
```sh
$ php app/check.php
```
Popolare il database con le tabelle necessarie
```sh
php app/console doctrine:schema:update --force
```
ed inserire alcuni dati nella tabella "general"
```sh
$ mysql -u root -p storesymfony < accogliweb.general.sql
```
Ripulire la cache degli ambienti dev e prod:
```sh
$ php app/console cache:clear --env=dev
$ php app/console cache:clear --env=prod
```

**Interventi su Apache**

Generazione di un certificato per poter utilizzare SSL su Apache:
```sh
$ openssl genrsa -des3 -out server.key 1024
$ openssl req -new -key server.key -out server.csr
$ cp server.key server.key.org
$ openssl rsa -in server.key.org -out server.key
$ openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

$ sudo cp server.crt /etc/ssl/certs
$ sudo cp server.key /etc/ssl/private
```
Abilitare il supporto SSL:
```sh
$ sudo a2enmod ssl
```
Creare per Apache il file di configurazione del sito ed abilitarlo:
```sh
$ sudo cd /etc/apache2/sites-available
$ sudo nano accogliweb.conf
```
ed inserire
```sh
<VirtualHost *:443>

	ServerName accogliweb.localhost

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/accogliweb/web
	<Directory /var/www/accogliweb/web>
	   AllowOverride All
	   Order Allow,Deny
	   Allow from All
	   <IfModule mod_rewrite.c>
              Options -MultiViews
              RewriteEngine On
              RewriteCond %{REQUEST_FILENAME} !-f
              RewriteRule ^(.*)$ app.php [QSA,L]
           </IfModule>
	</Directory>

	SSLEngine on

	SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire

	SSLCertificateFile /etc/ssl/certs/server.crt
	SSLCertificateKeyFile /etc/ssl/private/server.key

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
	
</VirtualHost>
```
quindi abilitare la configurazione
```sh
$ sudo a2ensite
```
Abilitare il modulo mod_rewrite:
```sh
$ sudo a2enmod rewrite
```
e riavviare Apache:
```sh
$ sudo service apache2 restart
```
Infine modificare il file /etc/hosts aggiungendo:
```sh
127.0.0.1	accogliweb.localhost
```
A questo punto l'applicazione dovrebbe essere disponibile attraverso l'URL: https://accogliweb.localhost/ 

*** *Fine* ***

<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;

//use Technomega\AccogliBundle\Entity\Cliente;
//use Technomega\AccogliBundle\Entity\General;
//use Technomega\AccogliBundle\Entity\Prenotazione;

class DocumentoController extends Controller
{
    public function make_C59Action(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('data', DateType::class, array('label'=>'Giorno di rilevamento',
                'widget' => 'single_text',
                'data' => new \DateTime('today')))
            ->add('Genera file xml', SubmitType::class)
            ->add('Genera report testuale', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            // Variabili utilizzate per il report xml
            $countI = array();
            $countE = array();
            $axml = array();

            // Variabili utilizzate per il report txt
            $tcountI = array();
            $tcountE = array();
            $atxt = array();

            $generalita = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:General')->find(1);
            $province = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Provincia');
            $nazioni = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Nazione');

            $prenotazioni = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Prenotazione');
            $clienti = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Cliente');

            $query = $prenotazioni->createQueryBuilder('p')
                ->where('p.datai < :datar')
                ->setParameter('datar', $data['data'])
                ->andwhere('p.dataf >= :datar')
                ->setParameter('datar', $data['data'])
                ->orderBy('p.dataf', 'ASC')
                ->getQuery();
            $prenotazioni_attive_ieri = $query->getResult();

            $query = $prenotazioni->createQueryBuilder('p')
                ->where('p.dataf = :datar')
                ->setParameter('datar', $data['data'])
                ->getQuery();
            $prenotazioni_partenza_oggi = $query->getResult();

            $query = $prenotazioni->createQueryBuilder('p')
                ->where('p.datai = :datar')
                ->setParameter('datar', $data['data'])
                ->getQuery();
            $prenotazioni_arrivi_oggi = $query->getResult();

            $nrprenotazioni_oggi = count($prenotazioni_attive_ieri)+count($prenotazioni_arrivi_oggi)-count($prenotazioni_partenza_oggi);
//        $nrpartenze = count($prenotazioni_partenza_oggi);
//        $nrarrivi = count($prenotazioni_arrivi_oggi);

//        $rigaxml = "\t<rigaTEST nrTotPartenze=\"".$nrpartenze."\"/>\n";
//        array_push($axml, $rigaxml);
//        $rigaxml = "\t<rigaTEST nrTotArrivi=\"".$nrarrivi."\"/>\n";
//        array_push($axml, $rigaxml);

            // Scorro le prenotazioni di utenti presenti già ieri
            foreach ($prenotazioni_attive_ieri as $prenotazione) {
                // Analizzo il capofamiglia/capogruppo
                $naz = $prenotazione->getCliente()->getCitndza();
                if ($naz == "ITALIA") {
                    $provs = $prenotazione->getCliente()->getProvr();
                    $provr = $province->findOneBy(array('sigla' => $prenotazione->getCliente()->getProvr()));
                    $res_id = $provr->getCodistat();
                    if (!isset($countI[$res_id])) {
                        $countI[$res_id] = array(1, 0, 0);
                        $tcountI[$provs] = array(1, 0, 0);
                    }
                    else {
                        $countI[$res_id][0] += 1;
                        $tcountI[$provs][0] += 1;
                    }
                } else {
                    $nazione = $nazioni->findOneBy(array('nome' => $naz));
                    $res_id = $nazione->getCodistat();
                    if (!isset($countE[$res_id])) {
                        $countE[$res_id] = array(1, 0, 0);
                        $tcountE[$naz] = array(1, 0, 0);
                    }
                    else {
                        $countE[$res_id][0] += 1;
                        $tcountE[$naz][0] += 1;
                    }
                }
                // Se presenti analizzo i componenti della famiglia/gruppo
                $membri = $prenotazione->getMembro();
                if (!empty($membri)) {
                    foreach ($membri as $membro) {
                        $id_membro = (int) substr($membro, 0, 5);
                        $naz = $clienti->find($id_membro)->getCitndza();
                        if ($naz == "ITALIA") {
                            $provs_membro = $clienti->find($id_membro)->getProvr();
                            $provr_membro = $province->findOneBy(array('sigla' => $clienti->find($id_membro)->getProvr()));
                            $res_id = $provr_membro->getCodistat();
                            if (!isset($countI[$res_id])) {
                                $countI[$res_id] = array(1, 0, 0);
                                $tcountI[$provs_membro] = array(1, 0, 0);
                            }
                            else {
                                $countI[$res_id][0] += 1;
                                $tcountI[$provs_membro][0] += 1;
                            }
                        } else {
                            $naz_membro = $nazioni->findOneBy(array('nome' => $naz));
                            $res_id = $naz_membro->getCodistat();
                            if (!isset($countE[$res_id])) {
                                $countE[$res_id] = array(1, 0, 0);
                                $tcountE[$naz] = array(1, 0, 0);
                            }
                            else {
                                $countE[$res_id][0] += 1;
                                $tcountE[$naz][0] += 1;
                            }
                        }
                    }
                }
            }

            // Scorro le prenotazioni di utenti arrivati oggi
            foreach ($prenotazioni_arrivi_oggi as $prenotazione) {
                // Analizzo il capofamiglia/capogruppo
                $naz = $prenotazione->getCliente()->getCitndza();
                if ($naz == "ITALIA") {
                    $provs = $prenotazione->getCliente()->getProvr();
                    if (!$provs) {
                        $this->get('session')
                            ->getFlashBag()
                            ->set('error', "ANAGRAFICA INCOMPLETA: inserire la Provincia di residenza del cliente "
                                .$prenotazione->getCliente()->getCognome()." ".$prenotazione->getCliente()->getNome());
                        return $this->redirect($this->generateUrl('technomega_accogli_cliente_modifica',
                            array('id' => $prenotazione->getCliente()->getId())));
                    }
                    $provincia = $province->findOneBy(array('sigla' => $prenotazione->getCliente()->getProvr()));
                    $res_id = $provincia->getCodistat();
                    if (!isset($countI[$res_id])) {
                        $countI[$res_id] = array(0, 1, 0);
                        $tcountI[$provs] = array(0, 1, 0);
                    } else {
                        $countI[$res_id][1] += 1;
                        $tcountI[$provs][1] += 1;
                    }
                } else {
                    $nazione = $nazioni->findOneBy(array('nome' => $naz));
                    $res_id = $nazione->getCodistat();
                    if (!isset($countE[$res_id])) {
                        $countE[$res_id] = array(0, 1, 0);
                        $tcountE[$naz] = array(0, 1, 0);
                    } else {
                        $countE[$res_id][1] += 1;
                        $tcountE[$naz][1] += 1;
                    }
                }
                // Se presenti analizzo i componenti della famiglia/gruppo
                $membri = $prenotazione->getMembro();
                if (!empty($membri)) {
                    foreach ($membri as $membro) {
                        $id_membro = (int) substr($membro, 0, 5);
                        $naz = $clienti->find($id_membro)->getCitndza();
                        if ($naz == "ITALIA") {
                            $provs = $clienti->find($id_membro)->getProvr();
                            $provr_membro = $province->findOneBy(array('sigla' => $clienti->find($id_membro)->getProvr()));
                            $res_id = $provr_membro->getCodistat();
                            if (!isset($countI[$res_id])) {
                                $countI[$res_id] = array(0, 1, 0);
                                $tcountI[$provs] = array(0, 1, 0);
                            }
                            else {
                                $countI[$res_id][1] += 1;
                                $tcountI[$provs][1] += 1;
                            }
                        } else {
                            $naz_membro = $nazioni->findOneBy(array('nome' => $naz));
                            $res_id = $naz_membro->getCodistat();
                            if (!isset($countE[$res_id])) {
                                $countE[$res_id] = array(0, 1, 0);
                                $tcountE[$naz] = array(0, 1, 0);
                            }
                            else {
                                $countE[$res_id][1] += 1;
                                $tcountE[$naz][1] += 1;
                            }
                        }
                    }
                }
            }

            // Scorro le prenotazioni di utenti in partenza oggi
            foreach ($prenotazioni_partenza_oggi as $prenotazione) {
                // Analizzo il capofamiglia/capogruppo
                $naz = $prenotazione->getCliente()->getCitndza();

                if ($naz == "ITALIA") {
                    $provs = $prenotazione->getCliente()->getProvr();
                    $provincia = $province->findOneBy(array('sigla' => $prenotazione->getCliente()->getProvr()));
                    $res_id = $provincia->getCodistat();
                    if (!isset($countI[$res_id])) {
                        $countI[$res_id] = array(0, 0, 1);
                        $tcountI[$provs] = array(0, 0, 1);
                    }
                    else {
                        $countI[$res_id][2] += 1;
                        $tcountI[$provs][2] += 1;
                    }
                } else {
                    $nazione = $nazioni->findOneBy(array('nome' => $naz));
                    $res_id = $nazione->getCodistat();
                    if (!isset($countE[$res_id])) {
                        $countE[$res_id] = array(0, 0, 1);
                        $tcountE[$naz] = array(0, 0, 1);
                    }
                    else {
                        $countE[$res_id][2] += 1;
                        $tcountE[$naz][2] += 1;
                    }
                }
                // Se presenti analizzo i componenti della famiglia/gruppo
                $membri = $prenotazione->getMembro();
                if (!empty($membri)) {
                    foreach ($membri as $membro) {
                        $id_membro = (int) substr($membro, 0, 5);
                        $naz = $clienti->find($id_membro)->getCitndza();
                        if ($naz == "ITALIA") {
                            $provs = $clienti->find($id_membro)->getProvr();
                            $provr_membro = $province->findOneBy(array('sigla' => $clienti->find($id_membro)->getProvr()));
                            $res_id = $provr_membro->getCodistat();
                            if (!isset($countI[$res_id])) {
                                $countI[$res_id] = array(0, 0, 1);
                                $tcountI[$provs] = array(0, 0, 1);
                            }
                            else {
                                $countI[$res_id][2] += 1;
                                $tcountI[$provs][2] += 1;
                            }
                        } else {
                            $naz_membro = $nazioni->findOneBy(array('nome' => $naz));
                            $res_id = $naz_membro->getCodistat();
                            if (!isset($countE[$res_id])) {
                                $countE[$res_id] = array(0, 0, 1);
                                $tcountE[$naz] = array(0, 0, 1);
                            }
                            else {
                                $countE[$res_id][2] += 1;
                                $tcountE[$naz][2] += 1;
                            }
                        }
                    }
                }
            }

            $idstrut = "\"" . $generalita->getIdistat() . "\"";
            $giorno = "\"" . date('Y-m-d', $data['data']->getTimestamp()) . "\"";
            $url1 = "\"http://www.regione.liguria.it/turismo/rimovcli\"";
            $url2 = "\"http://www.w3.org/2001/XMLSchema-instance\"";
            $nrcamere = "\"" . $generalita->getNrcamere() . "\"";
            $nrletti = "\"" . $generalita->getNrletti() . "\"";
            $denom = "\"ACCOGLIweb\"";

            $rigaxml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
            $rigatxt = "REPORT  Mod. C59  ISTAT     (sintesi)\n";
            array_push($axml, $rigaxml);
            array_push($atxt, $rigatxt);
            $rigaxml = "<rm:c59 idstruttura=" . $idstrut . " data=" . $giorno . " xmlns:rm=" . $url1 . " xmlns:xsi=" . $url2 . ">\n";
            $rigatxt = "Data: ". $giorno . "\n\n\n";
            array_push($axml, $rigaxml);
            array_push($atxt, $rigatxt);
            $rigaxml = "\t<mensile numcameredisp=" . $nrcamere . " numlettidisp=" . $nrletti . " softwaregestionale=" . $denom . "/>\n";
            array_push($axml, $rigaxml);
            // N. di camere (unità abitative) occupate;
            // attributo obbligatorio solo per Alberghi, Locande, Residenze d'epoca, RTA, Alberghi diffusi.
            // Uso il nr. di prenotazioni di oggi (assumo che ogni prenotazione si richiesta una camera)
            $rigaxml = "\t<giornaliero numcamereoccupate=\"" . $nrprenotazioni_oggi . "\">\n";
            array_push($axml, $rigaxml);

            // Compilo le righe con le presenze italiane (presenti = presenti ieri + arrivati - partiti)
            foreach ($countI as $i => $v) {
                $rigaxml = "\t\t<rigac59 nazione=\"" . "i" .
                    "\" residenza=\"" . $i .
                    "\" presenti=\"" . ($countI[$i][0]+$countI[$i][1]-$countI[$i][2]) .
                    "\" arrivati=\"" . $countI[$i][1] .
                    "\" partiti=\"" . $countI[$i][2] . "\"" . "/>\n";
                array_push($axml, $rigaxml);
            }

            $rigatxt = "Residenti ITALIA";
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 45)); array_push($atxt, "\n");
            $rigatxt = "PROVINCIA"." "."Pre.Ieri"." "."Arrivati"." "."Partiti"." "."Pre.Oggi";
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            $priI=0; $arrI=0; $parI=0; $proI=0;
            foreach ($tcountI as $i => $v) {
                $rigatxt = str_pad($i, 9, " ", STR_PAD_BOTH) . "  ".
                    str_pad($tcountI[$i][0], 8, " ", STR_PAD_BOTH) . " ".
                    str_pad($tcountI[$i][1], 7, " ", STR_PAD_BOTH) . " ".
                    str_pad($tcountI[$i][2], 7, " ", STR_PAD_BOTH) . "  ".
                    str_pad($tcountI[$i][0]+$tcountI[$i][1]-$tcountI[$i][2], 8, " ", STR_PAD_BOTH);
                array_push($atxt, $rigatxt);
                array_push($atxt, "\n");
                $priI+=$tcountI[$i][0]; $arrI+=$tcountI[$i][1];
                $parI+=$tcountI[$i][2]; $proI+=($tcountI[$i][0]+$tcountI[$i][1]-$tcountI[$i][2]);
            }
            array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 45)); array_push($atxt, "\n");
            $rigatxt = "Tot.       ".
                str_pad($priI, 8, " ", STR_PAD_BOTH)." ".str_pad($arrI, 7, " ", STR_PAD_BOTH)." ".
                str_pad($parI, 7, " ", STR_PAD_BOTH)."  ".str_pad($proI, 8, " ", STR_PAD_BOTH);
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 45)); array_push($atxt, "\n"); array_push($atxt, "\n");

            // Compilo le righe con le presenze estere (presenti = presenti ieri + arrivati - partiti)
            foreach ($countE as $i => $v) {
                $rigaxml = "\t\t<rigac59 nazione=\"" . "e" .
                    "\" residenza=\"" . $i .
                    "\" presenti=\"" . ($countE[$i][0]+$countE[$i][1]-$countE[$i][2]) .
                    "\" arrivati=\"" . $countE[$i][1] .
                    "\" partiti=\"" . $countE[$i][2] . "\"" . "/>\n";
                array_push($axml, $rigaxml);
            }

            $rigatxt = "Residenti ESTERO";
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 59)); array_push($atxt, "\n");
            $rigatxt = "PAESE ESTERO           "." "."Pre.Ieri"." "."Arrivati"." "."Partiti"." "."Pre.Oggi";
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            $priE=0; $arrE=0; $parE=0; $proE=0;
            foreach ($tcountE as $i => $v) {
                $rigatxt = str_pad($i, 25) .
                    str_pad($tcountE[$i][0], 7, " ", STR_PAD_BOTH) . "  ".
                    str_pad($tcountE[$i][1], 7, " ", STR_PAD_BOTH) . " ".
                    str_pad($tcountE[$i][2], 7, " ", STR_PAD_BOTH) . "  ".
                    str_pad($tcountE[$i][0]+$tcountE[$i][1]-$tcountE[$i][2], 7, " ", STR_PAD_BOTH);
                array_push($atxt, $rigatxt); array_push($atxt, "\n");
                $priE+=$tcountE[$i][0]; $arrE+=$tcountE[$i][1];
                $parE+=$tcountE[$i][2]; $proE+=($tcountE[$i][0]+$tcountE[$i][1]-$tcountE[$i][2]);
            }
            array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 59)); array_push($atxt, "\n");
            $rigatxt = "Tot.                     ".
                str_pad($priE, 7, " ", STR_PAD_BOTH)."  ".str_pad($arrE, 7, " ", STR_PAD_BOTH)." ".
                str_pad($parE, 7, " ", STR_PAD_BOTH)."  ".str_pad($proE, 7, " ", STR_PAD_BOTH);
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 59)); array_push($atxt, "\n"); array_push($atxt, "\n"); array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 45)); array_push($atxt, "\n");
            $rigatxt = "TOTALE   "." "."Pre.Ieri"." "."Arrivati"." "."Partiti"." "."Pre.Oggi";
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            $rigatxt = "           ".
                str_pad($priI+$priE, 8, " ", STR_PAD_BOTH)." ".str_pad($arrI+$arrE, 7, " ", STR_PAD_BOTH)." ".
                str_pad($parI+$parE, 7, " ", STR_PAD_BOTH)."  ".str_pad($proI+$proE, 8, " ", STR_PAD_BOTH);
            array_push($atxt, $rigatxt); array_push($atxt, "\n");
            array_push($atxt, str_repeat("-", 45)); array_push($atxt, "\n");

            $rigaxml = "\t</giornaliero>\n";
            array_push($axml, $rigaxml);
            $rigaxml = "</rm:c59>";
            array_push($axml, $rigaxml);

            $fgiorno = date('Ymd', $data['data']->getTimestamp());
            $pfname = 'mod_C59';

            // Creo il file in xml
            $response_xml = new StreamedResponse();
            $response_xml->setCallback(function () use ($axml) {
                foreach ($axml as $line) {
                    echo $line;
                    flush();
                }
            });
            $response_xml->headers->set('Content-Type', 'text/xml');
            $response_xml->headers->set('Content-Disposition', 'attachment; filename=' . $pfname."_".$fgiorno.".xml");

            // Creo il file txt
            $response_txt = new StreamedResponse();
            $response_txt->setCallback(function () use ($atxt) {
                foreach ($atxt as $line) {
                    echo $line;
                    flush();
                }
            });
            $response_txt->headers->set('Content-Type', 'text/plain');
            $response_txt->headers->set('Content-Disposition', 'attachment; filename=' . $pfname."_".$fgiorno.".tt");

            $response = $form->get('Genera report testuale')->isClicked() ? $response_txt : $response_xml;

            return $response;
        }

        return $this->render('TechnomegaAccogliBundle:Default:documento_creaModC59.html.php',
            array('form' => $form->createView(),));

    }

    public function make_docPSAction(Request $request) {

        $tipodoc = array(
            'IDENT' => 'Carta d\'Identità',
            'IDELE' => 'Carta d\'Id. Elettr.',
            'PATEN' => 'Patente di Guida',
            'PASOR' => 'Passaporto Ord.');

        $tipoallog = array(
            '0'  => 'Non specificato',
            '16' => 'Ospite Singolo',
            '17' => 'Capofamiglia',
            '18' => 'Capogruppo',
            '19' => 'Familiare',
            '20' => 'Membro Gruppo');

        $form = $this->createFormBuilder()
            ->add('data', DateType::class, array('label'=>'Giorno di rilevamento',
                'widget' => 'single_text',
                'data' => new \DateTime('today')))
            ->add('Genera doc. telematico', SubmitType::class)
            ->add('Genera doc. testuale', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $atxt = array();
            $atesto = array();

            $sexcode = array('M' => 1, 'F' => 2);

            //$generalita = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:General')->find(1);
            //$province = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Provincia');
            $nazioni = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Nazione');
            $comuni = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Comune');

            $prenotazioni = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Prenotazione');
            $clienti = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Cliente');

            $query = $prenotazioni->createQueryBuilder('p')
//            ->where('p.datai > :datai')
//            ->setParameter('datai', new \DateTime('today - 2 day'))
//            ->andwhere('p.dataf > :dataf')
//            ->setParameter('dataf', new \DateTime('today'))
                ->where('p.datai = :datai')
                ->setParameter('datai', $data['data'])
                ->orderBy('p.dataf', 'ASC')
                ->getQuery();
            $prenotazioni_attive = $query->getResult();

            $count = 0;
            // Scorro le prenotazioni di utenti presenti già ieri
            foreach ($prenotazioni_attive as $prenotazione) {

                // N.B. Il CR+LF non deve essere presente alla fine dell'ultima riga
                // allora lo inserisco solo prima di iniziare una riga successiva alla prima
                if ($count>0) {
                    $rigatxt = "\r\n";
                    array_push($atxt, $rigatxt);
                    $rigatesto = "\r\n";
                    array_push($atesto, $rigatesto);
                }

                // Analizzo il capofamiglia/capogruppo
                $tipo = $prenotazione->getCodcaf();
                $rigatxt = strval($tipo);
                array_push($atxt, $rigatxt);
                $rigatesto = "Tipo Alloggiato: ".$tipoallog[$tipo]."\r\n";
                array_push($atesto, $rigatesto);
                $rigatxt = $prenotazione->getDatai()->format('d/m/Y');
                array_push($atxt, $rigatxt);
                $rigatesto = "Data di arrivo: ".$rigatxt."\r\n";
                array_push($atesto, $rigatesto);
                $rigatxt = str_pad(strval(date_diff($prenotazione->getDataf(),
                    $prenotazione->getDatai())->days),
                    2, " ", STR_PAD_LEFT);
                array_push($atxt, $rigatxt);
                $rigatesto = "Nr. giorni permanenza: ".$rigatxt."\r\n";
                array_push($atesto, $rigatesto);

                //  Correzione x caratteri multi-byte
                $c_diff = strlen($prenotazione->getCliente()->getCognome())-mb_strlen($prenotazione->getCliente()->getCognome());

                $rigatxt = str_pad(strtoupper($prenotazione->getCliente()->getCognome()), 50+$c_diff, " ");
                array_push($atxt, $rigatxt);
                $rigatesto = "Cognome: ".$rigatxt."\r\n";
                array_push($atesto, $rigatesto);

                //  Correzione x caratteri multi-byte
                $n_diff = strlen($prenotazione->getCliente()->getNome())-mb_strlen($prenotazione->getCliente()->getNome());

                $rigatxt = str_pad(strtoupper($prenotazione->getCliente()->getNome()), 30+$n_diff, " ");
                array_push($atxt, $rigatxt);
                $rigatesto = "Nome: ".$rigatxt."\r\n";
                array_push($atesto, $rigatesto);
                $rigatxt = $sexcode[$prenotazione->getCliente()->getSex()];
                array_push($atxt, $rigatxt);
                $rigatesto = "Sesso: ".$prenotazione->getCliente()->getSex()."\r\n";
                array_push($atesto, $rigatesto);
                $rigatxt = $prenotazione->getCliente()->getDatan()->format('d/m/Y');
                array_push($atxt, $rigatxt);
                $rigatesto = "Data di nascita: ".$rigatxt."\r\n";
                array_push($atesto, $rigatesto);

                $naz = $prenotazione->getCliente()->getCitndza();
                $statn = $nazioni->findOneBy(array('nome' => $prenotazione->getCliente()->getStaton()));
                if ($statn == "ITALIA") {
                    if (!$prenotazione->getCliente()->getCittan()) {
                        $this->get('session')
                            ->getFlashBag()
                            ->set('error', "ANAGRAFICA INCOMPLETA: inserire il Comune di nascita del cliente "
                                .$prenotazione->getCliente()->getCognome()." ".$prenotazione->getCliente()->getNome());
                        return $this->redirect($this->generateUrl('technomega_accogli_cliente_modifica',
                            array('id' => $prenotazione->getCliente()->getId())));
                    }
                    $comur = $comuni->findOneBy(array('nome' => $prenotazione->getCliente()->getCittan()));
                    $com_id = $comur->getCodps();
                    $pro_id = $prenotazione->getCliente()->getProvn();
                    $rigatesto = "Luogo di nascita: ".$comur." (".$pro_id.")"."\r\n";
                } else {
                    $com_id = str_pad("", 9, " ");
                    $pro_id = str_pad("", 2, " ");
                    $rigatesto = "Luogo di nascita: ".$statn."\r\n";
                }
                array_push($atesto, $rigatesto);
                $rigatxt = $com_id;
                array_push($atxt, $rigatxt);
                $rigatxt = $pro_id;
                array_push($atxt, $rigatxt);

                $stn_id = $statn->getCodps();
                $rigatxt = $stn_id;
                array_push($atxt, $rigatxt);
                $citdz = $nazioni->findOneBy(array('nome' => $naz));
                $ctd_id = $citdz->getCodps();
                $rigatxt = $ctd_id;
                array_push($atxt, $rigatxt);
                $rigatesto = "Cittadinanza: ".$citdz."\r\n";
                array_push($atesto, $rigatesto);
                $doc_id = $prenotazione->getCliente()->getDoctip();
                $rigatxt = $doc_id;
                array_push($atxt, $rigatxt);
                $rigatesto = "Tipo Doc. Identità: ".$tipodoc[$doc_id]."\r\n";
                array_push($atesto, $rigatesto);
                $doc_nr = $prenotazione->getCliente()->getDocnum();
                $rigatxt = str_pad($doc_nr, 20, " ");
                array_push($atxt, $rigatxt);
                $rigatesto = "Nr. Doc. Identità: ".$rigatxt."\r\n";
                array_push($atesto, $rigatesto);

                if ($naz == "ITALIA" && ($doc_id == "IDENT" || $doc_id == "IDELE" || $doc_id == "PATEN")) {
                    $lrdoc = $comuni->findOneBy(array('nome' => $prenotazione->getCliente()->getDocloc()));
                    if (!$lrdoc) {
                        $this->get('session')
                            ->getFlashBag()
                            ->set('error', "ANAGRAFICA INCOMPLETA: inserire il luogo di rilascio del Doc. di Identità del cliente "
                                .$prenotazione->getCliente()->getCognome()." ".$prenotazione->getCliente()->getNome());
                        return $this->redirect($this->generateUrl('technomega_accogli_cliente_modifica',
                            array('id' => $prenotazione->getCliente()->getId())));
                    }

                    $lrd_id = $lrdoc->getCodps();
                    $rigatesto = "Luogo rilascio Doc.: ".$lrdoc."\r\n";
                } else {
                    $nazione = $nazioni->findOneBy(array('nome' => $prenotazione->getCliente()->getDocloc()));
                    if (!$nazione) {
                        $this->get('session')
                            ->getFlashBag()
                            ->set('error', "ANAGRAFICA INCOMPLETA: inserire il luogo di rilascio del Doc. di Identità del cliente "
                                .$prenotazione->getCliente()->getCognome()." ".$prenotazione->getCliente()->getNome());
                        return $this->redirect($this->generateUrl('technomega_accogli_cliente_modifica',
                            array('id' => $prenotazione->getCliente()->getId())));
                    }

                    $lrd_id = $nazione->getCodps();
                    $rigatesto = "Luogo rilascio Doc.: ".$nazione."\r\n";
                }
                array_push($atesto, $rigatesto);
                $rigatxt = $lrd_id;
                array_push($atxt, $rigatxt);
//                $rigatxt = "\n";
//                array_push($atxt, $rigatxt);

                // Se presenti analizzo i componenti della famiglia/gruppo
                $membri = $prenotazione->getMembro();
                if (!empty($membri)) {
                    foreach ($membri as $membro) {
                        $id_membro = (int) substr($membro, 0, 5);
                        $cliente = $clienti->find($id_membro);
                        $tipo = strval(intval($prenotazione->getCodcaf()) + 2);

                        $rigatxt = "\r\n";
                        array_push($atxt, $rigatxt);
                        $rigatesto = "\r\n";
                        array_push($atesto, $rigatesto);

                        $rigatxt = strval($tipo);
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Tipo Alloggiato: ".$tipoallog[$tipo]."\r\n";
                        array_push($atesto, $rigatesto);
                        $rigatxt = $prenotazione->getDatai()->format('d/m/Y');
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Data di arrivo: ".$rigatxt."\r\n";
                        array_push($atesto, $rigatesto);
                        $rigatxt = str_pad(strval(date_diff($prenotazione->getDataf(),
                            $prenotazione->getDatai())->days),
                            2, " ", STR_PAD_LEFT);
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Nr. giorni permanenza: ".$rigatxt."\r\n";
                        array_push($atesto, $rigatesto);
                        //  Correzione x caratteri multi-byte
                        $c_diff = strlen($cliente->getCognome())-mb_strlen($cliente->getCognome());
                        $rigatxt = str_pad(strtoupper($cliente->getCognome()), 50+$c_diff, " ");
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Cognome: ".$rigatxt."\r\n";
                        array_push($atesto, $rigatesto);
                        //  Correzione x caratteri multi-byte
                        $n_diff = strlen($cliente->getNome())-mb_strlen($cliente->getNome());
                        $rigatxt = str_pad(strtoupper($cliente->getNome()), 30+$n_diff, " ");
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Nome: ".$rigatxt."\r\n";
                        array_push($atesto, $rigatesto);
                        $rigatxt = $sexcode[$cliente->getSex()];
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Sesso: ".$cliente->getSex()."\r\n";
                        array_push($atesto, $rigatesto);
                        $rigatxt = $cliente->getDatan()->format('d/m/Y');
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Data di nascita: ".$rigatxt."\r\n";
                        array_push($atesto, $rigatesto);

                        $nazm = $cliente->getCitndza();
                        $statn = $nazioni->findOneBy(array('nome' => $cliente->getStaton()));
                        if ($statn == "ITALIA") {
                            if (!$cliente->getCittan()) {
                                $this->get('session')
                                    ->getFlashBag()
                                    ->set('error', "ANAGRAFICA INCOMPLETA: inserire il Comune di nascita del cliente "
                                        .$cliente->getCognome()." ".$cliente->getNome());
                                return $this->redirect(
                                    $this->generateUrl('technomega_accogli_cliente_modifica', array('id' => $cliente->getId())));
                            }
                            $comur = $comuni->findOneBy(array('nome' => $cliente->getCittan()));
                            $com_id = $comur->getCodps();
                            $pro_id = $cliente->getProvn();
                            $rigatesto = "   Luogo di nascita: ".$comur." (".$pro_id.")"."\r\n";
                        } else {
                            $com_id = str_pad("", 9, " ");
                            $pro_id = str_pad("", 2, " ");
                            $rigatesto = "   Luogo di nascita: ".$statn."\r\n";
                        }
                        array_push($atesto, $rigatesto);
                        $rigatxt = $com_id;
                        array_push($atxt, $rigatxt);
                        $rigatxt = $pro_id;
                        array_push($atxt, $rigatxt);

                        $stn_id = $statn->getCodps();
                        $rigatxt = $stn_id;
                        array_push($atxt, $rigatxt);
                        $citdz = $nazioni->findOneBy(array('nome' => $nazm));
                        $ctd_id = $citdz->getCodps();
                        $rigatxt = $ctd_id;
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Cittadinanza: ".$citdz."\r\n";
                        array_push($atesto, $rigatesto);
                        $doc_id = $cliente->getDoctip();
                        $rigatxt = $doc_id;
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Tipo Doc. Identità: ".$tipodoc[$doc_id]."\r\n";
                        array_push($atesto, $rigatesto);
                        $doc_nr = $cliente->getDocnum();
                        $rigatxt = str_pad($doc_nr, 20, " ");
                        array_push($atxt, $rigatxt);
                        $rigatesto = "   Nr. Doc. Identità: ".$rigatxt."\r\n";
                        array_push($atesto, $rigatesto);

                        if ($nazm == "ITALIA" && ($doc_id == "IDENT"  || $doc_id == "IDELE" || $doc_id == "PATEN")) {
                            $lrdoc = $comuni->findOneBy(array('nome' => $cliente->getDocloc()));
                            if (!$lrdoc) {
                                $this->get('session')
                                    ->getFlashBag()
                                    ->set('error', "ANAGRAFICA INCOMPLETA: inserire il luogo di rilascio del Doc. di Identità del cliente "
                                        .$cliente->getCognome()." ".$cliente->getNome());
                                return $this->redirect($this->generateUrl('technomega_accogli_cliente_modifica',
                                    array('id' => $cliente->getId())));
                            }

                            $lrd_id = $lrdoc->getCodps();
                            $rigatesto = "   Luogo rilascio Doc.: ".$lrdoc."\r\n";
                        } else {
                            $nazione = $nazioni->findOneBy(array('nome' => $cliente->getDocloc()));
                            if (!$nazione) {
                                $this->get('session')
                                    ->getFlashBag()
                                    ->set('error', "ANAGRAFICA INCOMPLETA: inserire il luogo di rilascio del Doc. di Identità del cliente "
                                        .$cliente->getCognome()." ".$cliente->getNome());
                                return $this->redirect($this->generateUrl('technomega_accogli_cliente_modifica',
                                    array('id' => $cliente->getId())));
                            }

                            $lrd_id = $nazione->getCodps();
                            $rigatesto = "   Luogo rilascio Doc.: ".$nazione."\r\n";
                        }
                        array_push($atesto, $rigatesto);
                        $rigatxt = $lrd_id;
                        array_push($atxt, $rigatxt);
//                        $rigatxt = "\n";
//                        array_push($atxt, $rigatxt);
                    }
                }
                array_push($atesto, "-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~");
                $count++;
            }

//        $response = $this->render('TechnomegaAccogliBundle:Default:list.csv.php',
//            array('afile' => $atxt));

            $response = new StreamedResponse();

            $response->setCallback(function () use ($atxt) {
                foreach ($atxt as $line) {
                    echo $line;
                    flush();
                }
            });

            $pfname = 'exportPS';
            $fgiorno = date('Ymd', $data['data']->getTimestamp());

            $response->headers->set('Content-Type', 'text/txt');
            $response->headers->set('Content-Disposition', 'attachment; filename=' . $pfname."_".$fgiorno.".txt");

            // Creo il file txt
            $pfname_testo = 'doctestualePS';
            $response_testo = new StreamedResponse();
            $response_testo->setCallback(function () use ($atesto) {
                foreach ($atesto as $line) {
                    echo $line;
                    flush();
                }
            });
            $response_testo->headers->set('Content-Type', 'text/plain');
            $response_testo->headers->set('Content-Disposition', 'attachment; filename=' . $pfname_testo."_".$fgiorno.".txt");

            return $form->get('Genera doc. telematico')->isClicked() ? $response : $response_testo;
        }

        return $this->render('TechnomegaAccogliBundle:Default:documento_creaDocPS.html.php',
            array('form' => $form->createView(),));
    }
}
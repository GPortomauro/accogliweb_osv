<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function defaultAction()
    {
        return $this->render('TechnomegaAccogliBundle:Default:base.html.php');
    }

    public function indexAction()
    {
        return $this->render('TechnomegaAccogliBundle:Default:index.html.php');
    }

    public function getSignboardAction()
    {
        $em = $this->getDoctrine()->getManager();
        $generalita =$em->getRepository('TechnomegaAccogliBundle:General')->find(1);
        $ditta = $generalita->getRagsoc1();
        $attivita = $generalita->getAttiv();

        return $this->render('TechnomegaAccogliBundle:Default:signboard.html.php',
            array('ditta' => $ditta, 'attivita' => $attivita));
    }
}

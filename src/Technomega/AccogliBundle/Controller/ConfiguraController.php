<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

use Technomega\AccogliBundle\Entity\General;

class ConfiguraController extends Controller
{
    public function configuraAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $general = $em->getRepository('TechnomegaAccogliBundle:General')->find(1);

        if (!$general)
            $general = new General();

        $tattiv = array(
            'Bad&Breakfast' => 'BB',
            'Camping' => 'CP',
            'Hotel' => 'HT',
            'Porto Turistico' => 'PT');

        $form = $this->createFormBuilder($general)
            ->add('ragsoc1', TextType::class, array('label'=>'Ragione Soc.',
                                                    'attr' => array('style' => 'width: 300px')))
            ->add('tipoattiv', ChoiceType::class, array('choices'=>$tattiv,
                                                        'choices_as_values' => true,
                                                        'label'=>'Tipologia',
                                                        'placeholder' => 'Selezionare'))
            ->add('attiv', TextType::class, array('label'=>'Descrizione dell\'attività',
                                            'attr' => array('style' => 'width: 230px')))
            ->add('idistat', TextType::class, array('label'=>'Cod. ISTAT',
                                            'attr' => array('style' => 'width: 80px')))
            ->add('indir1', TextType::class, array('label'=>'Indirizzo (Sed. Op.)',
                                            'attr' => array('style' => 'width: 200px')))
            ->add('cap1', TextType::class, array('label'=>'C.A.P.',
                                        'attr' => array('style' => 'width: 80px')))
            ->add('citta1', TextType::class, array('label'=>'Città'))
            ->add('prov1', TextType::class, array('label'=>'Provincia',
                                            'attr' => array('style' => 'width: 50px')))
            ->add('indir2', TextType::class, array('label'=>'Indirizzo (Sed. Leg.)',
                                            'attr' => array('style' => 'width: 200px')))
            ->add('cap2', TextType::class, array('label'=>'C.A.P.',
                                        'attr' => array('style' => 'width: 80px')))
            ->add('citta2', TextType::class, array('label'=>'Città'))
            ->add('prov2', TextType::class, array('label'=>'Provincia',
                                            'attr' => array('style' => 'width: 50px')))
            ->add('piva', TextType::class, array('label'=>'P. IVA'))
            ->add('cofi', TextType::class, array('label'=>'Cod. Fiscale', 'required' => false))
            ->add('tel1', TextType::class, array('label'=>'Telefono 1', 'required' => false))
            ->add('tel2', TextType::class, array('label'=>'Telefono 2', 'required' => false))
            ->add('fax', TextType::class, array('label'=>'FAX', 'required' => false))
            ->add('email1', TextType::class, array('label'=>'e-mail 1', 'required' => false,
                                            'attr' => array('style' => 'width: 250px')))
            ->add('email2', TextType::class, array('label'=>'e-mail 2', 'required' => false,
                                            'attr' => array('style' => 'width: 250px')))
            ->add('hpage', TextType::class, array('label'=>'Sito Web', 'required' => false,
                                            'attr' => array('style' => 'width: 300px')))
            ->add('nrcamere', NumberType::class, array('label'=>'Nr. camere',
                                                'attr' => array('style' => 'width: 50px')))
            ->add('nrletti', NumberType::class, array('label'=>'Nr. letti',
                                                'attr' => array('style' => 'width: 50px')))
            ->add('tassog', NumberType::class, array('label'=>'Tassa di Soggiorno [€]',
                'attr' => array('style' => 'width: 50px')))
            ->add('Salva', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($general);
            $em->flush();

            return $this->redirectToRoute('technomega_accogli');
        }

        return $this->render('TechnomegaAccogliBundle:Default:generalita_create.html.php',
            array('form' => $form->createView(),));

    }
}
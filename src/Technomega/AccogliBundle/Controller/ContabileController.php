<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Controller;

use DateInterval;
use DatePeriod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ContabileController extends Controller
{
    public function reportTassaSoggiornoAction(Request $request) {
        $form = $this->createFormBuilder()
            ->add('d_dal', DateType::class, array('label'=>'Dal giorno',
                'widget' => 'single_text',
                'data' => new \DateTime('today')))
            ->add('d_al', DateType::class, array('label'=>'Al giorno',
                'widget' => 'single_text',
                'data' => new \DateTime('today')))
            ->add('Genera', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $d_dal = clone $data['d_dal'];
            $d_al = clone $data['d_al'];
            $interv = DateInterval::createFromDateString('1 day');
            $periodo = new DatePeriod($d_dal, $interv, $d_al->add($interv));

            $generalita = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:General')->find(1);
            $tasSoggiorno = $generalita->getTassog();
            $clienti = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Cliente');

            $repository = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Prenotazione');

            // Array $report[ idcoll, nominativo, eta, tipo, esente, provenienza, provincia, datai, dataf, nper, importo ]
            $report = array();
            foreach ($periodo as $d_rilev) {
                $query = $repository->createQueryBuilder('p')
                    ->where('p.datai = :datai')
                    ->setParameter('datai', $d_rilev)
                    ->andwhere('p.dataf >= :datai')
                    ->setParameter('datai', $d_rilev)
                    ->orderBy('p.dataf', 'ASC')
                    ->getQuery();

                $prenotazioni_arrivi = $query->getResult();

                foreach ($prenotazioni_arrivi as $prenotazione) {
                    $id_coll = $prenotazione->getId();
                    $datainiziopren = $prenotazione->getDatai();
                    $datafinepren = $prenotazione->getDataf();
                    // Nr. Pernotti
                    $nPer = (($datafinepren->diff($d_rilev))->days) > 5 ? 5 : ($datafinepren->diff($d_rilev))->days;
                    $eta = (($prenotazione->getCliente()->getDatan())->diff($d_rilev))->y;
                    $tipo = $prenotazione->getPosto();
                    $esente = $prenotazione->getCliente()->getEts();
                    $nome_membro = $prenotazione->getCliente();
                    $nazionalita = $prenotazione->getCliente()->getCitndza();
                    $proven = ($nazionalita == "ITALIA") ? $prenotazione->getCliente()->getCittar() : $prenotazione->getCliente()->getCitndza();
                    $provincia = ($nazionalita == "ITALIA") ? $prenotazione->getCliente()->getProvr() : "";
                    if ($esente == 'S' || $eta < 15) {
                        $importo = 0.0;
                    } else {
                        // Importo dovuto = Nr. pernottamenti * tariffaSoggiorno
                        $importo = $nPer * $tasSoggiorno;
                    }
                    $report[] = [$id_coll, $nome_membro, $eta, $tipo, $esente, $proven, $provincia, $datainiziopren, $datafinepren, $nPer, $importo];

                    $membri = $prenotazione->getMembro();
                    if (!empty($membri)) {
                        foreach ($membri as $membro) {
                            $id_membro = (int)substr($membro, 0, 5);
                            $nome_membro = substr($membro, 5);
                            // Nr. Pernotti coincidono con quelli del Capo Famiglia o Capo Gruppo, quindi utilizzo quelli
                            $eta = (($clienti->find($id_membro)->getDatan())->diff($d_rilev))->y;
                            $esente = $clienti->find($id_membro)->getEts();
                            $nazionalita = $clienti->find($id_membro)->getCitndza();
                            $proven = ($nazionalita == "ITALIA") ? $clienti->find($id_membro)->getCittar() : $clienti->find($id_membro)->getCitndza();
                            $provincia = ($nazionalita == "ITALIA") ? $clienti->find($id_membro)->getProvr() : "";
                            if ($esente == 'S' || $eta < 15) {
                                $importo = 0.0;
                            } else {
                                // Importo dovuto = Nr. pernottamenti * tariffaSoggiorno
                                $importo = $nPer * $tasSoggiorno;
                            }
                            $report[] = [$id_coll, $nome_membro, $eta, $tipo, $esente, $proven, $provincia, $datainiziopren, $datafinepren, $nPer, $importo];
                        }
                    }
                }
            }

            return $this->render('TechnomegaAccogliBundle:Default:contabile_report_TS.html.php',
                array('report_prenotazioni' => $report, 'd_dal' => $data['d_dal'], 'd_al' => $data['d_al']));
        }
        return $this->render('TechnomegaAccogliBundle:Default:contabile_insdatarilevamento_TS.html.php',
            array('form' => $form->createView(),));
    }

}
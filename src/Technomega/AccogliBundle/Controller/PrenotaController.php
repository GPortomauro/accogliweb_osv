<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Technomega\AccogliBundle\Entity\Prenotazione;
//use Technomega\AccogliBundle\Entity\Cliente;
use Technomega\AccogliBundle\Entity\Prenotazioneold;
use Technomega\AccogliBundle\Form\Type\PrenotazioneType;

class PrenotaController extends Controller
{
    public function creaAction(Request $request)
    {
        $prenotazione = new Prenotazione();
        if ($request->query->get('idcliente')) {
            $cliente = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Cliente')->find($request->query->get('idcliente'));
            $prenotazione->setCliente($cliente);
        }
        $form = $this->createForm(new PrenotazioneType($this->getDoctrine()->getManager()), $prenotazione);
                        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // Verifico l'assenza di anomalie nell'impostazione delle date
            if ($prenotazione->getDataf() < $prenotazione->getDatai()) {
                $this->get('session')
                    ->getFlashBag()
                    ->set('notice', "Attenzione! La data finale deve essere posteriore alla data iniziale.");
                return $this->redirect($this->generateUrl('technomega_accogli_prenota',
                    array('idcliente' => $prenotazione->getCliente()->getId())));
            }
            // Verifico che non si tenti di reinserire una registrazione
            if ($prenotazione_old = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Prenotazione')
                ->findBy(array('cliente' => $prenotazione->getCliente(),
                    'datai' => $prenotazione->getDatai(),
                    'dataf' => $prenotazione->getDataf()))) {
                $this->get('session')
                    ->getFlashBag()
                    ->set('notice', "Attenzione! Esiste già una registrazione per ".$prenotazione->getCliente()." con quel periodo.");
                return $this->redirect($this->generateUrl('technomega_accogli_prenota',
                    array('idcliente' => $prenotazione->getCliente()->getId())));
            }
            $em->persist($prenotazione);
            $em->flush();
            
            return $this->mostraAction($prenotazione->getId());
                //$this->crea_schedaAction($prenotazione->getId());
                //$this->redirect($this->generateUrl('technomega_accogli_prenotazioni_attive'));
        }
      
        return $this->render('TechnomegaAccogliBundle:Default:prenotazione_create.html.php',
                                array('form' => $form->createView(),));
    
    }

    public function cancellaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $prenotazione = $em->getRepository('TechnomegaAccogliBundle:Prenotazione')->find($id);

        if (!$prenotazione) {
            throw $this->createNotFoundException('Nessuna prenotazione trovata per l\'id '.$id);
         }

        $em->remove($prenotazione);
        $em->flush();
        return $this->redirect($this->generateUrl('technomega_accogli_prenotazioni_attive'));
    }

    public function modificaAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prenotazione = $em->getRepository('TechnomegaAccogliBundle:Prenotazione')->find($id);

        if (!$prenotazione) {
            throw $this->createNotFoundException('Nessuna prenotazione trovata per l\'id '.$id);
        }

        $form = $this->createForm(new PrenotazioneType($this->getDoctrine()->getManager()), $prenotazione);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($prenotazione);
            $em->flush();
            return $this->redirect($this->generateUrl('technomega_accogli_prenotazioni_attive'));
        }
        return $this->render('TechnomegaAccogliBundle:Default:prenotazione_create.html.php',
                                array('form' => $form->createView(),));
    }

    public function crea_schedaAction($id) {

        $em = $this->getDoctrine()->getManager();
        $prenotazione = $em->getRepository('TechnomegaAccogliBundle:Prenotazione')->find($id);

        $cliente = $em->getRepository('TechnomegaAccogliBundle:Cliente')->find($prenotazione->getCliente()->getId());

        $atxt = array();

        $tipoposti = array(
            'B' => 'Bungalow',
            'T' => 'Tenda',
            'R' => 'Roulotte',
            'P' => 'Piazzola',
            'MR' => 'MarinaResort');

        $tipoallog = array(
            '0'  => 'Non specificato',
            '16' => 'Ospite Singolo',
            '17' => 'Capofamiglia',
            '18' => 'Capogruppo',
            '19' => 'Familiare',
            '20' => 'Membro Gruppo');

        $tipodoc = array(
            'IDENT' => 'Carta d\' Identità',
            'IDELE' => 'Carta d\' Id. Elettr.',
            'PATEN' => 'Patente di Guida',
            'PASOR' => 'Passaporto Ord.');

        array_push($atxt, "\n");
        array_push($atxt, "\n");
        array_push($atxt, str_repeat(" ", 50)."Prenotazione n. ".$id."\n");
        array_push($atxt, str_repeat(" ", 50)."Data di arrivo: ".$prenotazione->getDatai()->format('d-m-Y')."\n");
        array_push($atxt, "\n");
        array_push($atxt, "\n");
        array_push($atxt, "Cliente: ".$prenotazione->getCliente()."\n");
        array_push($atxt, "\n");
        array_push($atxt, "Nato a: ".str_pad($cliente->getCittan(), 40, " ")."\n");
        array_push($atxt, "Il: ".$cliente->getDatan()->format('d-m-Y')."  Prov.: ".$cliente->getProvn()."\n");
        array_push($atxt, "Residente in: ".str_pad($cliente->getIndir(), 40, " ")."\n");
        array_push($atxt, "Città: ".str_pad($cliente->getCittar(), 40, " ")."\n");
        array_push($atxt, "Prov.: ".$cliente->getProvr()."         Nazionalità: ".$cliente->getCitndza()."\n");

        array_push($atxt, "Tipo Documento: ".str_pad($tipodoc[$cliente->getDoctip()], 5, " ")
            ."   Nr. ".$cliente->getDocnum()."\n");
        array_push($atxt, "Luogo (Comune o Stato): ".str_pad($cliente->getDocloc(), 40, " ")."\n");
        array_push($atxt, "Data rilascio: ".$cliente->getDocdat()->format('d-m-Y')."\n");

        array_push($atxt, str_repeat(" ", 67)."Tipo alloggiato: ".$tipoallog[$prenotazione->getCodcaf()]."\n");
        array_push($atxt, str_repeat(" ", 67)."Membri gruppo: \n");
        foreach ($prenotazione->getMembro() as $membro)
            array_push($atxt, str_repeat(" ", 67).substr($membro, 6)."\n");
        array_push($atxt, "\n");
        array_push($atxt, "\n");
        array_push($atxt, " ".$prenotazione->getCliente()
            ."  Posto ".$tipoposti[$prenotazione->getPosto()]
            ." ".$prenotazione->getNumpos()
            ."   Dal ".$prenotazione->getDatai()->format('d-m-Y')
            ." al ".$prenotazione->getDataf()->format('d-m-Y')
            ."   ( ".sprintf("%s", count($prenotazione->getMembro())+1)." persone )");

//        $response = $this->render('TechnomegaAccogliBundle:Default:list.csv.php',
//            array('afile' => $atxt));

        $pfname = 'ACCOGLIweb_scheda_'.$id;

//        $response = new StreamedResponse();
//
//        $response->setCallback(function () use($atxt) {
//            foreach ($atxt as $line) {
//                echo $line;
//                flush();
//            }
//        });
//
//        $response->headers->set('Content-Type', 'text/txt');
//        $response->headers->set('Content-Disposition', 'attachment; filename='.$pfname."_".$id.".tt");
//
//        return $response;

        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator('ACCOGLIweb');
        $pdf->SetAuthor($this->getUser());
        $pdf->SetTitle($pfname);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 9, '', true);
        $pdf->AddPage();

        $html = "<pre>";
        foreach ($atxt as $line) {
            $html = $html.$line;
        }
        $html = $html."</pre>";

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '',
            $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        return new StreamedResponse(function () use ($pdf, $pfname) {
            $pdf->Output($pfname.".pdf");
        });
    }

    public function mostraAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $prenotazione = $em->getRepository('TechnomegaAccogliBundle:Prenotazione')->find($id);

        if (!$prenotazione) {
            throw $this->createNotFoundException('Nessuna prenotazione trovata per l\'id '.$id);
        }
        return $this->render('TechnomegaAccogliBundle:Default:prenotazione_show.html.php',
                                array ('prenotazione' => $prenotazione));
    }

    public function list_allAction($page = 1)
    {
        $repository = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Prenotazione');

        $prenotazioni = $repository->findAll();

        $limit = 25;
        $totprenotazioni = count($prenotazioni);
        // Massimo numero di pagine risultanti
        $maxpages = ceil($totprenotazioni/$limit);
        $thispage = $page;

        $pageprenotazioni = $repository->findBy(array(), array('id' => 'ASC'), $limit, $limit * ($page - 1) );

        /*$provenienze = array();

        foreach ($prenotazioni as $prenotazione) {
            $nazionalita = $prenotazione->getCliente()->getCitndza();

            $provenienza = ($nazionalita == "ITALIA") ?
                $prenotazione->getCliente()->getCittar() : $prenotazione->getCliente()->getCitndza();

            array_push($provenienze, $provenienza);
        }*/

        return $this->render('TechnomegaAccogliBundle:Default:prenotazioni_listAll.html.php',
            array ('prenotazioni' => $pageprenotazioni, 'maxpages' => $maxpages, 'thispage' => $thispage));
    }

    public function list_activeAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('data', DateType::class, array('label'=>'Al giorno',
                'widget' => 'single_text',
                'data' => new \DateTime('today')))
            ->add('Visualizza', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $repository = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Prenotazione');

            $query = $repository->createQueryBuilder('p')
                ->where('p.datai <= :datar')
                ->setParameter('datar', $data['data'])
                ->andwhere('p.dataf >= :datar')
                ->setParameter('datar', $data['data'])
                ->orderBy('p.dataf', 'ASC')
                ->getQuery();

            $prenotazioni_attive = $query->getResult();

            /*$provenienze = array();

            foreach ($prenotazioni_attive as $prenotazione) {
                $nazionalita = $prenotazione->getCliente()->getCitndza();

                $provenienza = ($nazionalita == "ITALIA") ?
                    $prenotazione->getCliente()->getCittar() : $prenotazione->getCliente()->getCitndza();

                array_push($provenienze, $provenienza);
            }*/

            return $this->render('TechnomegaAccogliBundle:Default:prenotazioni_listActive.html.php',
                array('prenotazioni' => $prenotazioni_attive, 'datar' => ($data['data'])->format('d-m-Y')));
        }
        return $this->render('TechnomegaAccogliBundle:Default:prenotazione_insdatarilevamento_presenze.html.php',
            array('form' => $form->createView(),));
    }

    public function list_scadenzaAction(Request $request) {
        $form = $this->createFormBuilder()
            ->add('data', DateType::class, array('label'=>'Al giorno',
                'widget' => 'single_text',
                'data' => new \DateTime('today')))
            ->add('Visualizza', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $repository = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Prenotazione');

            $query = $repository->createQueryBuilder('p')
                ->where('p.dataf = :datar')
                ->setParameter('datar', $data['data'])
                ->orderBy('p.cliente', 'ASC')
                ->getQuery();

            $prenotazioni_inscadenza = $query->getResult();

            return $this->render('TechnomegaAccogliBundle:Default:prenotazioni_listInScadenza.html.php',
                array('prenotazioni' => $prenotazioni_inscadenza, 'datar' => ($data['data'])->format('d-m-Y')));
        }
        return $this->render('TechnomegaAccogliBundle:Default:prenotazione_insdatarilevamento_scadenze.html.php',
            array('form' => $form->createView(),));
    }

    public function cercaAction(Request $request) {
        $defaultData = array('message' => 'Inserisci il nominativo');
        $form = $this->createFormBuilder($defaultData)
            ->add('cognome', TextType::class, array('label'=>'Cognome'))
            ->add('nome', TextType::class, array('label'=>'Nome', 'required' => false))
            ->add('db', ChoiceType::class, array(
                'choices' => array('Gest. corrente' => 'C', 'Gest. passate' => 'P'),
//                'expanded' => true,
                'choices_as_values' => true,
                'label' => 'Prenotazioni'))
            ->add('Cerca', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            if ($data['db'] == 'C')
                $dbprenotazioni = 'TechnomegaAccogliBundle:Prenotazione';
            else
                $dbprenotazioni = 'TechnomegaAccogliBundle:Prenotazioneold';

            $query = $this->getDoctrine()->getRepository($dbprenotazioni)
                ->createQueryBuilder('p')
                ->select('c', 'p')
                ->join('p.cliente', 'c' )
                ->where('c.cognome LIKE :cognome')->setParameter('cognome', $data['cognome']."%");
            if (isset($data['nome'])) {
                $query->andwhere('c.nome LIKE :nome')->setParameter('nome', $data['nome']."%");
            }
            $query->addOrderBy('p.datai', 'ASC');

            $prenotazioni = $query->getQuery()->getResult();

            if ($data['db'] == 'C')
                return $this->render('TechnomegaAccogliBundle:Default:prenotazioni_listatrovate.html.php',
                    array('prenotazioni' => $prenotazioni));
            else
                return $this->render('TechnomegaAccogliBundle:Default:prenotazioni_old_listatrovate.html.php',
                    array('prenotazioni' => $prenotazioni));
        }

        return $this->render('TechnomegaAccogliBundle:Default:prenotazione_cerca.html.php',
            array('form' => $form->createView(),));
    }

    public function azzeraAction() {

        $em = $this->getDoctrine()->getManager();
        $prenotazioni = $em->getRepository('TechnomegaAccogliBundle:Prenotazione')->findAll();

        // Copio il contenuto di Prenotazione in Prenotazioneold
        $batchSize = 20;
        $i = 0;
        foreach ($prenotazioni as $prenotazione) {
            $prenotazione_old = new Prenotazioneold();
            $prenotazione_old->setCliente($prenotazione->getCliente())
                ->setCodcaf($prenotazione->getCodcaf())
                ->setDatai($prenotazione->getDatai())
                ->setDataf($prenotazione->getDataf())
                ->setPosto($prenotazione->getPosto())
                ->setDatains($prenotazione->getDatains())
                ->setMembro($prenotazione->getMembro());
            $em->persist($prenotazione_old);
            if (($i % $batchSize) === 0) {
                $em->flush();
            }
            $i++;
        }
        $em->flush();
        $em->clear();

        // Eseguo l'azzeramento di Prenotazione
        $cmd = $em->getClassMetadata('TechnomegaAccogliBundle:Prenotazione');
        $connection = $em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');

//        $this->get('session')
//            ->getFlashBag()
//            ->set('notice', "Tabella DB delle prenotazioni azzerata");
        return $this->redirect($this->generateUrl('technomega_accogli'));
    }
}



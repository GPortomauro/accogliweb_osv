<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
//        $session = $request->getSession();
//
//        // verifica di eventuali errori
//        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
//            $error = $request->attributes->get(
//                Security::AUTHENTICATION_ERROR
//            );
//        } elseif (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
//            $error = $session->get(Security::AUTHENTICATION_ERROR);
//            $session->remove(Security::AUTHENTICATION_ERROR);
//        } else {
//            $error = '';
//        }
//
//        // ultimo nome utente inserito
//        $lastUsername = (null === $session) ? '' : $session->get(Security::LAST_USERNAME);

        $helper = $this->get('security.authentication_utils');

        return $this->render(
            'TechnomegaAccogliBundle:Security:login.html.php',
            array(
                // ultimo nome utente inserito
                'last_username' => $helper->getLastUsername(),             //$lastUsername,
                'error'         => $helper->getLastAuthenticationError(),  //$error,
            )
        );
    }
}
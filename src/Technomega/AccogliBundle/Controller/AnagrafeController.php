<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Technomega\AccogliBundle\Entity\Cliente;
use Technomega\AccogliBundle\Form\Type\ClienteType;

class AnagrafeController extends Controller
{
    public function inserisciAction(Request $request)
    {
        $cliente = new Cliente();

        $cliente->setCognome($request->query->get('cognome'));
        $cliente->setNome($request->query->get('nome'));
        $form = $this->createForm(new ClienteType($this->getDoctrine()->getManager()), $cliente);

        $form->handleRequest($request);

        if ($form->isValid()) {
            // Converto l'indirizzo, il nr. documento e le note in caratteri maiuscoli
            $cliente->setIndir(mb_strtoupper($cliente->getIndir()));
            $cliente->setDocnum(mb_strtoupper($cliente->getDocnum()));
            $cliente->setNote(mb_strtoupper($cliente->getNote()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();
            $nextAction = $form->get('Salva e Prenota')
                               ->isClicked() ? 'technomega_accogli_prenota' : 'technomega_accogli';
            return $this->redirect($this->generateUrl($nextAction, array('idcliente' => $cliente->getId())));
        }       
        return $this->render('TechnomegaAccogliBundle:Default:cliente_create.html.php',
                                    array('form' => $form->createView(),));
    }

    public function inseriscinuovoAction(Request $request)
    {
        $defaultData = array('message' => 'Inserisci il nominativo');
        $form = $this->createFormBuilder($defaultData)
            ->add('cognome', TextType::class, array('label'=>'Cognome'))
            ->add('nome', TextType::class, array('label'=>'Nome'))
            ->add('Avanti', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            // Trasformo in caratteri maiuscoli il nominativo
            $data['cognome'] = mb_strtoupper($data['cognome']);
            $data['nome'] = mb_strtoupper($data['nome']);

            $clienti = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Cliente');
            $query = $clienti->createQueryBuilder('p')
                ->where('p.cognome LIKE :cognomecliente')
                ->setParameter('cognomecliente', $data['cognome']);
            if (isset($data['nome'])) {
                $query->andwhere('p.nome LIKE :nomecliente')
                    ->setParameter('nomecliente', $data['nome']);
            }
            $query->orderBy('p.nome', 'ASC');

            $clienti_trovati = $query->getQuery()->getResult();

            if ($clienti_trovati) {
                $this->get('session')
                    ->getFlashBag()
                    ->set('notice', "Il nominativo \"".$data['cognome']." ".$data['nome']."\" è già presente negli archivi");
                return $this->redirect($this->generateUrl('technomega_accogli_cliente_inseriscinuovo'));
            }

            return $this->redirect($this->generateUrl('technomega_accogli_cliente_inserisci',
                array('cognome' => $data['cognome'], 'nome' => $data['nome'])));
        }

        return $this->render('TechnomegaAccogliBundle:Default:cliente_verifica_nominativo.html.php',
            array('form' => $form->createView(),));
    }

    public function cancellaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $cliente = $em->getRepository('TechnomegaAccogliBundle:Cliente')->find($id);

        if (!$cliente) {
            throw $this->createNotFoundException('Nessun cliente trovato per l\'id '.$id);
        }

        // Se sono presenti prenotazioni interrompo la cancellazione e segnalo la necessità di cancellarle
        $coll_pre = $cliente->getPrenotazioni();
        if (!$coll_pre->isEmpty()) {
            $this->get('session')
                ->getFlashBag()
                ->set('error', "ATTENZIONE! Per quel cliente sono presenti collocazioni/prenotazioni, non è possibile rimuoverne l'anagrafica");
            return $this->redirect($this->generateUrl('technomega_accogli'));
        }

        $em->remove($cliente);
        $em->flush();
        return $this->redirect($this->generateUrl('technomega_accogli'));
    }

    public function modificaAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cliente = $em->getRepository('TechnomegaAccogliBundle:Cliente')->find($id);

        if (!$cliente) {
            throw $this->createNotFoundException('Nessun cliente trovato per l\'id '.$id);
        }

        $form = $this->createForm(new ClienteType($this->getDoctrine()->getManager()), $cliente);

        $form->handleRequest($request);
        if ($form->isValid()) {
            // Converto l'indirizzo, il nr. documento e le note in caratteri maiuscoli
            $cliente->setIndir(mb_strtoupper($cliente->getIndir()));
            $cliente->setDocnum(mb_strtoupper($cliente->getDocnum()));
            $cliente->setNote(mb_strtoupper($cliente->getNote()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();
            return $this->redirect($this->generateUrl('technomega_accogli_cliente_mostra', array('id' => $id)));
        }
        return $this->render('TechnomegaAccogliBundle:Default:cliente_modify.html.php',
                                    array('form' => $form->createView(),));
    }

    public function mostraAction($id)
    {
        $cliente = $this->getDoctrine()
                        ->getRepository('TechnomegaAccogliBundle:Cliente')
                        ->find($id);

        if (!$cliente) {
            throw $this->createNotFoundException('Nessun cliente trovato per l\'id '.$id);
        }
        return $this->render('TechnomegaAccogliBundle:Default:cliente_show.html.php', array ('cliente' => $cliente));
    }
    
    public function listAction($page = 1)
    {
        $repository = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Cliente');

        $clienti = $repository->findAll();

        $limit = 35;
        $totclienti = count($clienti);
        // Massimo numero di pagine risultanti
        $maxpages = ceil($totclienti/$limit);
        $thispage = $page;

        $pageclienti = $repository->findBy(array(), array('cognome' => 'ASC'), $limit, $limit * ($page - 1));

        return $this->render('TechnomegaAccogliBundle:Default:clienti_list.html.php',
            array ('clienti' => $pageclienti, 'maxpages' => $maxpages, 'thispage' => $thispage));
    }

    public function cercaAction(Request $request) {
        $defaultData = array('message' => 'Inserisci il nominativo');
        $form = $this->createFormBuilder($defaultData)
            ->add('cognome', TextType::class, array('label'=>'Cognome'))
            ->add('nome', TextType::class, array('label'=>'Nome', 'required' => false))
            ->add('Cerca', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $clienti = $this->getDoctrine()->getRepository('TechnomegaAccogliBundle:Cliente');
            $query = $clienti->createQueryBuilder('p')
                ->where('p.cognome LIKE :cognomecliente')
                ->setParameter('cognomecliente', $data['cognome']."%");
            if (isset($data['nome'])) {
                $query->andwhere('p.nome LIKE :nomecliente')
                    ->setParameter('nomecliente', $data['nome']."%");
            }
            $query->addOrderBy('p.cognome', 'ASC')
                ->addOrderBy('p.nome', 'ASC');

            $clienti_trovati = $query->getQuery()->getResult();

            if (!$clienti_trovati) {
                //throw $this->createNotFoundException('Nessun cliente trovato');
                $this->get('session')
                    ->getFlashBag()
                    ->set('notice', "Il nominativo \"".$data['cognome']." ".$data['nome']."\" non è presente");
                return $this->redirect($this->generateUrl('technomega_accogli_cerca'));
            }

            return $this->render('TechnomegaAccogliBundle:Default:clienti_listatrovati.html.php',
                array ('clienti' => $clienti_trovati));
        }

        return $this->render('TechnomegaAccogliBundle:Default:cliente_cerca.html.php',
            array('form' => $form->createView(),));
    }
    
}


<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Technomega\AccogliBundle\Entity\Comune;
use Technomega\AccogliBundle\Entity\Nazione;
use Technomega\AccogliBundle\Entity\Provincia;

//use Technomega\AccogliBundle\Entity\Cliente;

class ClienteType extends AbstractType
{
    protected $em;
    protected $listprov;
    protected $listnaz;
    protected $listcom;

    function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $province = $this->em->getRepository('TechnomegaAccogliBundle:Provincia')->findBy(array(), array('sigla' => 'ASC'));
        $this->listprov = array();
        foreach ($province as $provincia) {
            $sigla = $provincia->getSigla();
            $this->listprov[$sigla] = $sigla;
        }

        $nazioni = $this->em->getRepository('TechnomegaAccogliBundle:Nazione')->findAll();
        $this->listnaz = array();
        foreach ($nazioni as $nazione) {
            $name = rtrim($nazione->getNome());
            $this->listnaz[$name] = $name;
        }

        $comuni = $this->em->getRepository('TechnomegaAccogliBundle:Comune')->findBy(array(),array('nome' => 'ASC'));
        $this->listcom;
        foreach($comuni as $comune) {
            $name = rtrim($comune->getNome());
            $this->listcom[$name] = $name;
        }

        $tipodoc = array(
            'IDENT' => 'Carta d\' Identità',
            'IDELE' => 'Carta d\' Id. Elettr.',
            'PATEN' => 'Patente di Guida',
            'PASOR' => 'Passaporto Ord.');

        $builder
            ->add('cognome', TextType::class, array('attr' => array('style' => 'width: 200px')))
            ->add('nome', TextType::class, array('attr' => array('style' => 'width: 200px')))
            ->add('sex', ChoiceType::class, array(
                'choices' => array(
                    'Maschio' => 'M',
                    'Femmina' => 'F'),
                'expanded'    => true,
                'choices_as_values' => true,
                'label'       => 'Sesso',
                'required'    => true,))
            ->add('staton', ChoiceType::class, array(
                'choices' => $this->listnaz,
                'choices_as_values' => true,
                'label' => 'Naz. di nascita',
                'required'    => true,
                'placeholder' => 'Selezionare'))
            ->add('datan', DateType::class, array('label'=>'Data di nascita',
                'required'=>true, 'format' => \IntlDateFormatter::SHORT, 'years' => range(date('Y')-99, date('Y'))))
            ->add('citndza', ChoiceType::class, array(
                'choices' => $this->listnaz,
                'choices_as_values' => true,
                'label' => 'Cittadinanza (Stato)',
                'required'    => true,
                'placeholder' => 'Selezionare'))
            ->add('indir', TextType::class, array('label' => 'Indir. di residenza', 'required' => false,
                                            'attr' => array('style' => 'width: 200px')))
            ->add('doctip', ChoiceType::class, array(
                'choices' => $tipodoc,
                'choices_as_values' => false,
                'label'       => 'Tipo documento',
                'required'    => true,
                'placeholder' => 'Selezionare'))
            ->add('docnum', TextType::class, array('label' => 'Nr. Documento'))
            ->add('docdat', DateType::class, array('label' => 'Data di rilascio',
                'format' => \IntlDateFormatter::SHORT, 'years' => range(date('Y')-100, date('Y'))))
            ->add('note', TextType::class, array('label' => 'Note', 'required' => false,
                                        'attr' => array('style' => 'width: 400px')))
            ->add('ets', ChoiceType::class, array(
                'label' => 'Esenzione Tassa Soggiorno',
                'choices' => array(
                    'N' => 'No',
                    'S' => 'Sì')))
            ->add('datins', DateType::class, array('label' => 'Data di inserimento','data' => new \DateTime('now')))

            ->add('Salva', SubmitType::class)
            ->add('Salva e Prenota', SubmitType::class);

            $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
            $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    public function onPreSetData(FormEvent $event)
    {
        $user = $event->getData();
        $form = $event->getForm();

        if (!$user) {
            return;
        }

        if ($user->getStaton() == "ITALIA") {
            $form
                ->add('cittan', ChoiceType::class, array(
                    'choices' => $this->listcom,
                    'choices_as_values' => true,
                    'label' => 'Comune di nascita',
                    'placeholder' => 'Selezionare'))
                ->add('provn', ChoiceType::class, array(
                    'choices' => $this->listprov,
                    'choices_as_values' => true,
                    'label' => 'Prov. di nascita',
                    'placeholder' => 'Selezionare'))
                ->add('cittar', ChoiceType::class, array(
                    'choices' => $this->listcom,
                    'choices_as_values' => true,
                    'label' => 'Città di residenza',
                    'required'    => false,
                    'placeholder' => 'Selezionare'))
                ->add('provr', ChoiceType::class, array(
                    'choices' => $this->listprov,
                    'choices_as_values' => true,
                    'label' => 'Prov. di residenza',
                    'required'    => false,
                    'placeholder' => 'Selezionare'));
        } else {
            $form->add('cittan', ChoiceType::class, array(
                'choices' => $this->listcom,
                'choices_as_values' => true,
                'label' => 'Comune di nascita',
                'required'    => false,
                'placeholder' => 'Selezionare'))
                ->add('provn', ChoiceType::class, array(
                    'choices' => $this->listprov,
                    'choices_as_values' => true,
                    'label' => 'Prov. di nascita',
                    'required'    => false,
                    'placeholder' => 'Selezionare'))
                ->add('cittar', ChoiceType::class, array(
                    'choices' => $this->listcom,
                    'choices_as_values' => true,
                    'label' => 'Città di residenza',
                    'required'    => false,
                    'placeholder' => 'Selezionare'))
                ->add('provr', ChoiceType::class, array(
                    'choices' => $this->listprov,
                    'choices_as_values' => true,
                    'label' => 'Prov. di residenza',
                    'required'    => false,
                    'placeholder' => 'Selezionare'));
        }

        if (($user->GetCitndza() == "ITALIA" && strcmp($user->GetDoctip(), 'PASOR') !== 0)
                || $user->GetCitndza() == null) {
            $form
                ->add('docloc', ChoiceType::class, array(
                    'choices' => $this->listcom,
                    'choices_as_values' => true,
                    'label' => 'Luogo di rilascio (Comune)',
                    'placeholder' => 'Selezionare'));
        } else if (($user->GetCitndza() == "ITALIA" && strcmp($user->GetDoctip(), 'PASOR') == 0)
            || $user->GetCitndza() !== "ITALIA") {
            $form->add('docloc', ChoiceType::class, array(
                'choices' => $this->listnaz,
                'choices_as_values' => true,
                'label' => 'Luogo di rilascio (Nazione)',
                'placeholder' => 'Selezionare'));
        }
    }

    public function onPreSubmit(FormEvent $event) {
        $user = $event->getData();
        $form = $event->getForm();

        if (!$user) {
            return;
        }

        if ($user['staton'] == "ITALIA") {
            $form
                ->add('cittan', ChoiceType::class, array(
                    'choices' => $this->listcom,
                    'choices_as_values' => true,
                    'label' => 'Comune di nascita',
                    'placeholder' => 'Selezionare'))
                ->add('provn', ChoiceType::class, array(
                    'choices' => $this->listprov,
                    'choices_as_values' => true,
                    'label' => 'Prov. di nascita',
                    'placeholder' => 'Selezionare'))
                ->add('cittar', ChoiceType::class, array(
                    'choices' => $this->listcom,
                    'choices_as_values' => true,
                    'label' => 'Città di residenza',
                    'required'    => false,
                    'placeholder' => 'Selezionare'))
                ->add('provr', ChoiceType::class, array(
                    'choices' => $this->listprov,
                    'choices_as_values' => true,
                    'label' => 'Prov. di residenza',
                    'required'    => false,
                    'placeholder' => 'Selezionare'));
        } else {
            $form->add('cittan', TextType::class, array(
                'required' => false,
                'label' => 'Comune di nascita',
                'empty_data'  => ''))
                ->add('provn', TextType::class, array(
                    'label' => 'Prov. di nascita',
                    'required'    => false))
                ->add('cittar', TextType::class, array(
                    'label' => 'Città di residenza',
                    'required'    => false))
                ->add('provr', TextType::class, array(
                    'label' => 'Prov. di residenza',
                    'required'    => false));;
        }

        if ($user['citndza'] == "ITALIA" && strcmp($user['doctip'], 'PASOR') !== 0 ){
            $form
                ->add('docloc', ChoiceType::class, array(
                    'choices' => $this->listcom,
                    'choices_as_values' => true,
                    'label' => 'Luogo di rilascio (Comune)',
                    'placeholder' => 'Selezionare'));
        } else {
            $form->add('docloc', ChoiceType::class, array(
                'choices' => $this->listnaz,
                'choices_as_values' => true,
                'label' => 'Luogo di rilascio (Nazione)',
                'placeholder' => 'Selezionare'));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Technomega\AccogliBundle\Entity\Cliente',));
    }
}

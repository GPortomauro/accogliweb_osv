<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Technomega\AccogliBundle\Entity\Cliente;

class PrenotazioneType extends AbstractType
{
    protected $em;
    protected $list;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $clienti = $this->em
            ->getRepository('TechnomegaAccogliBundle:Cliente')
            ->findBy(array(), array('cognome' => 'ASC'));
        foreach($clienti as $cliente){
            $name = str_pad(strval($cliente->getId()), 5, "0", STR_PAD_LEFT)  /* ID del cliente */
                ." ".$cliente->getCognome()
                ." ".$cliente->getNome();
            $this->list[$name] = $name;
        }

        $tipoallog = array(
            'Ospite Singolo' => '16',
            'Capofamiglia' => '17',
            'Capogruppo' => '18',
            'Familiare' => '19',
            'Membro Gruppo' => '20');

        $tipoposti = array(
            'Bungalow' => 'B',
            'Tenda' => 'T',
            'Roulotte' => 'R',
            'Camper' => 'C',
            'Stanziale' => 'S',
            'MarinaResort' => 'MR');

        $builder
            ->add('cliente', null, array(
                'choices_as_values' => true,
                'label' => 'Cliente',
                'required' => true,
                'placeholder' => 'Selezionare'))
            ->add('codcaf', ChoiceType::class, array(
                'choices' => $tipoallog,
                'choices_as_values' => true,
                'label'       => 'Tipol. allog.',
                'required'    => true,
                'placeholder' => 'Selezionare'))
            ->add('posto', ChoiceType::class, array(
                'choices' => $tipoposti,
                'choices_as_values' => true,
                'label' => 'Tipo',
                'required' => true,
                'placeholder' => 'Selezionare'))
            ->add('numpos', TextType::class, array('label' => 'Posiz./Nr.', 'required' => false,
                'attr' => array('style' => 'width: 70px')))
            ->add('membro', ChoiceType::class, array(
                'label' => 'Membri del gruppo',
                'choices' => $this->list,
                'choices_as_values' => true,
                'multiple' => true,
                'required' => false,))
            ->add('datains', DateType::class, array('label'=>'Data di inserimento',
                'data' => new \DateTime('now')))
            ->add('Salva', SubmitType::class);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
    }

    public function onPreSetData(FormEvent $event) {
        $prenotazione = $event->getData();
        $form = $event->getForm();

        if (!$prenotazione) {
            return;
        }

        $prenotazione->getDatai() == null ?
            $form->add('datai', DateType::class, array(
                'widget' => 'single_text',
                'label'=>'Data inizio',
                'data' => new \DateTime('today'))) :
            $form->add('datai', 'date', array('widget' => 'single_text', 'label'=>'Data inizio'/*, 'format' => \IntlDateFormatter::SHORT*/));

        $prenotazione->getDataf() == null ?
            $form->add('dataf', DateType::class, array(
                'widget' => 'single_text',
                'label'=>'Data fine',
                'data' => new \DateTime('tomorrow'))) :
            $form->add('dataf', DateType::class, array('widget' => 'single_text', 'label'=>'Data fine'/*, 'format' => \IntlDateFormatter::SHORT*/));
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Technomega\AccogliBundle\Entity\Prenotazione',));
                //->setRequired(array('repos',));
                //->setAllowedTypes(array('repo' => 'Doctrine\Common\Persistence\ObjectManager',));
    }
}

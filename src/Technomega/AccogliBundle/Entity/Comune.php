<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Entity;

/**
 * Comune
 */
class Comune
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $codps;

    /**
     * @var string
     */
    private $nome;

    /**
     * @var string
     */
    private $prov;

    /**
     * @var string
     */
    private $datafinval;

    /**
     *
     * @return string String representation of this class
     */
    public function __toString()
    {
        return $this->nome;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codps
     *
     * @param string $codps
     *
     * @return Comune
     */
    public function setCodps($codps)
    {
        $this->codps = $codps;

        return $this;
    }

    /**
     * Get codps
     *
     * @return string
     */
    public function getCodps()
    {
        return $this->codps;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Comune
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set prov
     *
     * @param string $prov
     *
     * @return Comune
     */
    public function setProv($prov)
    {
        $this->prov = $prov;

        return $this;
    }

    /**
     * Get prov
     *
     * @return string
     */
    public function getProv()
    {
        return $this->prov;
    }

    /**
     * Set datafinval
     *
     * @param string $datafinval
     *
     * @return Comune
     */
    public function setDatafinval($datafinval)
    {
        $this->datafinval = $datafinval;

        return $this;
    }

    /**
     * Get datafinval
     *
     * @return string
     */
    public function getDatafinval()
    {
        return $this->datafinval;
    }
}

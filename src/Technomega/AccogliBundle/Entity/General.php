<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Entity;

/**
 * General
 */
class General
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ragsoc1;

    /**
     * @var string
     */
    private $ragsoc2;

    /**
     * @var string
     */
    private $attiv;

    /**
     * @var string
     */
    private $indir1;

    /**
     * @var string
     */
    private $cap1;

    /**
     * @var string
     */
    private $citta1;

    /**
     * @var string
     */
    private $prov1;

    /**
     * @var string
     */
    private $indir2;

    /**
     * @var string
     */
    private $cap2;

    /**
     * @var string
     */
    private $citta2;

    /**
     * @var string
     */
    private $prov2;

    /**
     * @var string
     */
    private $piva;

    /**
     * @var string
     */
    private $cofi;

    /**
     * @var string
     */
    private $tel1;

    /**
     * @var string
     */
    private $tel2;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var string
     */
    private $email1;

    /**
     * @var string
     */
    private $email2;

    /**
     * @var string
     */
    private $hpage;

    /**
     * @var string
     */
    private $tassog;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ragsoc1
     *
     * @param string $ragsoc1
     *
     * @return General
     */
    public function setRagsoc1($ragsoc1)
    {
        $this->ragsoc1 = $ragsoc1;

        return $this;
    }

    /**
     * Get ragsoc1
     *
     * @return string
     */
    public function getRagsoc1()
    {
        return $this->ragsoc1;
    }

    /**
     * Set ragsoc2
     *
     * @param string $ragsoc2
     *
     * @return General
     */
    public function setRagsoc2($ragsoc2)
    {
        $this->ragsoc2 = $ragsoc2;

        return $this;
    }

    /**
     * Get ragsoc2
     *
     * @return string
     */
    public function getRagsoc2()
    {
        return $this->ragsoc2;
    }

    /**
     * Set attiv
     *
     * @param string $attiv
     *
     * @return General
     */
    public function setAttiv($attiv)
    {
        $this->attiv = $attiv;

        return $this;
    }

    /**
     * Get attiv
     *
     * @return string
     */
    public function getAttiv()
    {
        return $this->attiv;
    }

    /**
     * Set indir1
     *
     * @param string $indir1
     *
     * @return General
     */
    public function setIndir1($indir1)
    {
        $this->indir1 = $indir1;

        return $this;
    }

    /**
     * Get indir1
     *
     * @return string
     */
    public function getIndir1()
    {
        return $this->indir1;
    }

    /**
     * Set cap1
     *
     * @param string $cap1
     *
     * @return General
     */
    public function setCap1($cap1)
    {
        $this->cap1 = $cap1;

        return $this;
    }

    /**
     * Get cap1
     *
     * @return string
     */
    public function getCap1()
    {
        return $this->cap1;
    }

    /**
     * Set citta1
     *
     * @param string $citta1
     *
     * @return General
     */
    public function setCitta1($citta1)
    {
        $this->citta1 = $citta1;

        return $this;
    }

    /**
     * Get citta1
     *
     * @return string
     */
    public function getCitta1()
    {
        return $this->citta1;
    }

    /**
     * Set prov1
     *
     * @param string $prov1
     *
     * @return General
     */
    public function setProv1($prov1)
    {
        $this->prov1 = $prov1;

        return $this;
    }

    /**
     * Get prov1
     *
     * @return string
     */
    public function getProv1()
    {
        return $this->prov1;
    }

    /**
     * Set indir2
     *
     * @param string $indir2
     *
     * @return General
     */
    public function setIndir2($indir2)
    {
        $this->indir2 = $indir2;

        return $this;
    }

    /**
     * Get indir2
     *
     * @return string
     */
    public function getIndir2()
    {
        return $this->indir2;
    }

    /**
     * Set cap2
     *
     * @param string $cap2
     *
     * @return General
     */
    public function setCap2($cap2)
    {
        $this->cap2 = $cap2;

        return $this;
    }

    /**
     * Get cap2
     *
     * @return string
     */
    public function getCap2()
    {
        return $this->cap2;
    }

    /**
     * Set citta2
     *
     * @param string $citta2
     *
     * @return General
     */
    public function setCitta2($citta2)
    {
        $this->citta2 = $citta2;

        return $this;
    }

    /**
     * Get citta2
     *
     * @return string
     */
    public function getCitta2()
    {
        return $this->citta2;
    }

    /**
     * Set prov2
     *
     * @param string $prov2
     *
     * @return General
     */
    public function setProv2($prov2)
    {
        $this->prov2 = $prov2;

        return $this;
    }

    /**
     * Get prov2
     *
     * @return string
     */
    public function getProv2()
    {
        return $this->prov2;
    }

    /**
     * Set piva
     *
     * @param string $piva
     *
     * @return General
     */
    public function setPiva($piva)
    {
        $this->piva = $piva;

        return $this;
    }

    /**
     * Get piva
     *
     * @return string
     */
    public function getPiva()
    {
        return $this->piva;
    }

    /**
     * Set cofi
     *
     * @param string $cofi
     *
     * @return General
     */
    public function setCofi($cofi)
    {
        $this->cofi = $cofi;

        return $this;
    }

    /**
     * Get cofi
     *
     * @return string
     */
    public function getCofi()
    {
        return $this->cofi;
    }

    /**
     * Set tel1
     *
     * @param string $tel1
     *
     * @return General
     */
    public function setTel1($tel1)
    {
        $this->tel1 = $tel1;

        return $this;
    }

    /**
     * Get tel1
     *
     * @return string
     */
    public function getTel1()
    {
        return $this->tel1;
    }

    /**
     * Set tel2
     *
     * @param string $tel2
     *
     * @return General
     */
    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;

        return $this;
    }

    /**
     * Get tel2
     *
     * @return string
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return General
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email1
     *
     * @param string $email1
     *
     * @return General
     */
    public function setEmail1($email1)
    {
        $this->email1 = $email1;

        return $this;
    }

    /**
     * Get email1
     *
     * @return string
     */
    public function getEmail1()
    {
        return $this->email1;
    }

    /**
     * Set email2
     *
     * @param string $email2
     *
     * @return General
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set hpage
     *
     * @param string $hpage
     *
     * @return General
     */
    public function setHpage($hpage)
    {
        $this->hpage = $hpage;

        return $this;
    }

    /**
     * Get hpage
     *
     * @return string
     */
    public function getHpage()
    {
        return $this->hpage;
    }
    /**
     * @var string
     */
    private $tipoattiv;

    /**
     * @var string
     */
    private $idistat;

    /**
     * @var integer
     */
    private $nrcamere;

    /**
     * @var integer
     */
    private $nrletti;


    /**
     * Set tipoattiv
     *
     * @param string $tipoattiv
     *
     * @return General
     */
    public function setTipoattiv($tipoattiv)
    {
        $this->tipoattiv = $tipoattiv;

        return $this;
    }

    /**
     * Get tipoattiv
     *
     * @return string
     */
    public function getTipoattiv()
    {
        return $this->tipoattiv;
    }

    /**
     * Set idistat
     *
     * @param string $idistat
     *
     * @return General
     */
    public function setIdistat($idistat)
    {
        $this->idistat = $idistat;

        return $this;
    }

    /**
     * Get idistat
     *
     * @return string
     */
    public function getIdistat()
    {
        return $this->idistat;
    }

    /**
     * Set nrcamere
     *
     * @param integer $nrcamere
     *
     * @return General
     */
    public function setNrcamere($nrcamere)
    {
        $this->nrcamere = $nrcamere;

        return $this;
    }

    /**
     * Get nrcamere
     *
     * @return integer
     */
    public function getNrcamere()
    {
        return $this->nrcamere;
    }

    /**
     * Set nrletti
     *
     * @param integer $nrletti
     *
     * @return General
     */
    public function setNrletti($nrletti)
    {
        $this->nrletti = $nrletti;

        return $this;
    }

    /**
     * Get nrletti
     *
     * @return integer
     */
    public function getNrletti()
    {
        return $this->nrletti;
    }

    /**
     * Set tassog
     *
     * @param string $tassog
     *
     * @return General
     */
    public function setTassog($tassog)
    {
        $this->tassog = $tassog;

        return $this;
    }

    /**
     * Get tassog
     *
     * @return string
     */
    public function getTassog()
    {
        return $this->tassog;
    }
}

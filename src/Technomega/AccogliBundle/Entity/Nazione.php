<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Entity;

/**
 * Nazione
 */
class Nazione
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $codistat;

    /**
     * @var string
     */
    private $nome;

    /**
     *
     * @return string String representation of this class
     */
    public function __toString()
    {
        return $this->nome;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codistat
     *
     * @param string $codistat
     *
     * @return Nazione
     */
    public function setCodistat($codistat)
    {
        $this->codistat = $codistat;

        return $this;
    }

    /**
     * Get codistat
     *
     * @return string
     */
    public function getCodistat()
    {
        return $this->codistat;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Nazione
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
    /**
     * @var string
     */
    private $codps;


    /**
     * Set codps
     *
     * @param string $codps
     *
     * @return Nazione
     */
    public function setCodps($codps)
    {
        $this->codps = $codps;

        return $this;
    }

    /**
     * Get codps
     *
     * @return string
     */
    public function getCodps()
    {
        return $this->codps;
    }
}

<?php

/*
* Copyright (C) 2014-2016 Giancarlo Portomauro
*
* This file is part of ACCOGLIweb project.
*
* ACCOGLIweb is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ACCOGLIweb is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ACCOGLIweb. For the full copyright and license information,
* please view the LICENSE file that was distributed with this source code.
* If not, see <http://www.gnu.org/licenses/>.
*/

namespace Technomega\AccogliBundle\Entity;

/**
 * Provincia
 */
class Provincia
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $codistat;

    /**
     * @var string
     */
    private $nome;

    /**
     * @var string
     */
    private $sigla;

    /**
     * @var string
     */
    private $codreg;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codistat
     *
     * @param string $codistat
     *
     * @return Provincia
     */
    public function setCodistat($codistat)
    {
        $this->codistat = $codistat;

        return $this;
    }

    /**
     * Get codistat
     *
     * @return string
     */
    public function getCodistat()
    {
        return $this->codistat;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Provincia
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set sigla
     *
     * @param string $sigla
     *
     * @return Provincia
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;

        return $this;
    }

    /**
     * Get sigla
     *
     * @return string
     */
    public function getSigla()
    {
        return $this->sigla;
    }

    /**
     * Set codreg
     *
     * @param string $codreg
     *
     * @return Provincia
     */
    public function setCodreg($codreg)
    {
        $this->codreg = $codreg;

        return $this;
    }

    /**
     * Get codreg
     *
     * @return string
     */
    public function getCodreg()
    {
        return $this->codreg;
    }
}

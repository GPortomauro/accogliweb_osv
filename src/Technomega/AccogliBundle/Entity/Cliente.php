<?php

namespace Technomega\AccogliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity
 */
class Cliente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cognome", type="string", length=255)
     */
    private $cognome;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=1)
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="cittan", type="string", length=40, nullable=true)
     */
    private $cittan;

    /**
     * @var string
     *
     * @ORM\Column(name="provn", type="string", length=2, nullable=true)
     */
    private $provn;

    /**
     * @var string
     *
     * @ORM\Column(name="staton", type="string", length=40)
     */
    private $staton;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datan", type="date")
     */
    private $datan;

    /**
     * @var string
     *
     * @ORM\Column(name="citndza", type="string", length=40)
     */
    private $citndza;

    /**
     * @var string
     *
     * @ORM\Column(name="indir", type="string", length=30, nullable=true)
     */
    private $indir;

    /**
     * @var string
     *
     * @ORM\Column(name="cittar", type="string", length=40, nullable=true)
     */
    private $cittar;

    /**
     * @var string
     *
     * @ORM\Column(name="provr", type="string", length=2, nullable=true)
     */
    private $provr;

    /**
     * @var string
     *
     * @ORM\Column(name="doctip", type="string", length=5)
     */
    private $doctip;

    /**
     * @var string
     *
     * @ORM\Column(name="docnum", type="string", length=15)
     */
    private $docnum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="docdat", type="date")
     */
    private $docdat;

    /**
     * @var string
     *
     * @ORM\Column(name="docloc", type="string", length=40)
     */
    private $docloc;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=120, nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="ets", type="string", length=1, nullable=true)
     */
    private $ets;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datins", type="datetime")
     */
    private $datins;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Technomega\AccogliBundle\Entity\Prenotazione", mappedBy="cliente")
     */
    private $prenotazioni;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Technomega\AccogliBundle\Entity\Prenotazioneold", mappedBy="cliente")
     */
    private $prenotazioniold;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prenotazioni = new \Doctrine\Common\Collections\ArrayCollection();
        $this->prenotazioniold = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     *  
     * @return string String representation of this class
     */
    public function __toString()
    {
        return $this->cognome." ".$this->nome;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cognome
     *
     * @param string $cognome
     *
     * @return Cliente
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;

        return $this;
    }

    /**
     * Get cognome
     *
     * @return string
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Cliente
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set sex
     *
     * @param string $sex
     *
     * @return Cliente
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set cittan
     *
     * @param string $cittan
     *
     * @return Cliente
     */
    public function setCittan($cittan)
    {
        $this->cittan = $cittan;

        return $this;
    }

    /**
     * Get cittan
     *
     * @return string
     */
    public function getCittan()
    {
        return $this->cittan;
    }

    /**
     * Set provn
     *
     * @param string $provn
     *
     * @return Cliente
     */
    public function setProvn($provn)
    {
        $this->provn = $provn;

        return $this;
    }

    /**
     * Get provn
     *
     * @return string
     */
    public function getProvn()
    {
        return $this->provn;
    }

    /**
     * Set staton
     *
     * @param string $staton
     *
     * @return Cliente
     */
    public function setStaton($staton)
    {
        $this->staton = $staton;

        return $this;
    }

    /**
     * Get staton
     *
     * @return string
     */
    public function getStaton()
    {
        return $this->staton;
    }

    /**
     * Set datan
     *
     * @param \DateTime $datan
     *
     * @return Cliente
     */
    public function setDatan($datan)
    {
        $this->datan = $datan;

        return $this;
    }

    /**
     * Get datan
     *
     * @return \DateTime
     */
    public function getDatan()
    {
        return $this->datan;
    }

    /**
     * Set citndza
     *
     * @param string $citndza
     *
     * @return Cliente
     */
    public function setCitndza($citndza)
    {
        $this->citndza = $citndza;

        return $this;
    }

    /**
     * Get citndza
     *
     * @return string
     */
    public function getCitndza()
    {
        return $this->citndza;
    }

    /**
     * Set indir
     *
     * @param string $indir
     *
     * @return Cliente
     */
    public function setIndir($indir)
    {
        $this->indir = $indir;

        return $this;
    }

    /**
     * Get indir
     *
     * @return string
     */
    public function getIndir()
    {
        return $this->indir;
    }

    /**
     * Set cittar
     *
     * @param string $cittar
     *
     * @return Cliente
     */
    public function setCittar($cittar)
    {
        $this->cittar = $cittar;

        return $this;
    }

    /**
     * Get cittar
     *
     * @return string
     */
    public function getCittar()
    {
        return $this->cittar;
    }

    /**
     * Set provr
     *
     * @param string $provr
     *
     * @return Cliente
     */
    public function setProvr($provr)
    {
        $this->provr = $provr;

        return $this;
    }

    /**
     * Get provr
     *
     * @return string
     */
    public function getProvr()
    {
        return $this->provr;
    }

    /**
     * Set doctip
     *
     * @param string $doctip
     *
     * @return Cliente
     */
    public function setDoctip($doctip)
    {
        $this->doctip = $doctip;

        return $this;
    }

    /**
     * Get doctip
     *
     * @return string
     */
    public function getDoctip()
    {
        return $this->doctip;
    }

    /**
     * Set docnum
     *
     * @param string $docnum
     *
     * @return Cliente
     */
    public function setDocnum($docnum)
    {
        $this->docnum = $docnum;

        return $this;
    }

    /**
     * Get docnum
     *
     * @return string
     */
    public function getDocnum()
    {
        return $this->docnum;
    }

    /**
     * Set docdat
     *
     * @param \DateTime $docdat
     *
     * @return Cliente
     */
    public function setDocdat($docdat)
    {
        $this->docdat = $docdat;

        return $this;
    }

    /**
     * Get docdat
     *
     * @return \DateTime
     */
    public function getDocdat()
    {
        return $this->docdat;
    }

    /**
     * Set docloc
     *
     * @param string $docloc
     *
     * @return Cliente
     */
    public function setDocloc($docloc)
    {
        $this->docloc = $docloc;

        return $this;
    }

    /**
     * Get docloc
     *
     * @return string
     */
    public function getDocloc()
    {
        return $this->docloc;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Cliente
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set ets
     *
     * @param string $ets
     *
     * @return Cliente
     */
    public function setEts($ets)
    {
        $this->ets = $ets;

        return $this;
    }

    /**
     * Get ets
     *
     * @return string
     */
    public function getEts()
    {
        return $this->ets;
    }

    /**
     * Set datins
     *
     * @param \DateTime $datins
     *
     * @return Cliente
     */
    public function setDatins($datins)
    {
        $this->datins = $datins;

        return $this;
    }

    /**
     * Get datins
     *
     * @return \DateTime
     */
    public function getDatins()
    {
        return $this->datins;
    }

    /**
     * Add prenotazioni
     *
     * @param \Technomega\AccogliBundle\Entity\Prenotazione $prenotazioni
     *
     * @return Cliente
     */
    public function addPrenotazioni(\Technomega\AccogliBundle\Entity\Prenotazione $prenotazioni)
    {
        $this->prenotazioni[] = $prenotazioni;

        return $this;
    }

    /**
     * Remove prenotazioni
     *
     * @param \Technomega\AccogliBundle\Entity\Prenotazione $prenotazioni
     */
    public function removePrenotazioni(\Technomega\AccogliBundle\Entity\Prenotazione $prenotazioni)
    {
        $this->prenotazioni->removeElement($prenotazioni);
    }

    /**
     * Get prenotazioni
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrenotazioni()
    {
        return $this->prenotazioni;
    }

    /**
     * Add prenotazioniold
     *
     * @param \Technomega\AccogliBundle\Entity\Prenotazioneold $prenotazioniold
     *
     * @return Cliente
     */
    public function addPrenotazioniold(\Technomega\AccogliBundle\Entity\Prenotazioneold $prenotazioniold)
    {
        $this->prenotazioniold[] = $prenotazioniold;

        return $this;
    }

    /**
     * Remove prenotazioniold
     *
     * @param \Technomega\AccogliBundle\Entity\Prenotazioneold $prenotazioniold
     */
    public function removePrenotazioniold(\Technomega\AccogliBundle\Entity\Prenotazioneold $prenotazioniold)
    {
        $this->prenotazioniold->removeElement($prenotazioniold);
    }

    /**
     * Get prenotazioniold
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrenotazioniold()
    {
        return $this->prenotazioniold;
    }
}

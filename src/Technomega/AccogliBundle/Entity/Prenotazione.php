<?php

namespace Technomega\AccogliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prenotazione
 *
 * @ORM\Table(name="prenotazione")
 * @ORM\Entity
 */
class Prenotazione
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="codcaf", type="integer", length=2)
     */
    private $codcaf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datai", type="date")
     */
    private $datai;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataf", type="date")
     */
    private $dataf;

    /**
     * @var string
     *
     * @ORM\Column(name="posto", type="string", length=4)
     */
    private $posto;

    /**
     * @var string
     *
     * @ORM\Column(name="numpos", type="string", length=5, nullable=true)
     */
    private $numpos;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datains", type="datetime")
     */
    private $datains;

    /**
     * @var array
     *
     * @ORM\Column(name="membro", type="array", length=255)
     */
    private $membro;

    /**
     * @var \Technomega\AccogliBundle\Entity\Cliente
     *
     * @ORM\ManyToOne(targetEntity="Technomega\AccogliBundle\Entity\Cliente", inversedBy="prenotazioni")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     * })
     */
    private $cliente;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codcaf
     *
     * @param integer $codcaf
     *
     * @return Prenotazione
     */
    public function setCodcaf($codcaf)
    {
        $this->codcaf = $codcaf;

        return $this;
    }

    /**
     * Get codcaf
     *
     * @return integer
     */
    public function getCodcaf()
    {
        return $this->codcaf;
    }

    /**
     * Set datai
     *
     * @param \DateTime $datai
     *
     * @return Prenotazione
     */
    public function setDatai($datai)
    {
        $this->datai = $datai;

        return $this;
    }

    /**
     * Get datai
     *
     * @return \DateTime
     */
    public function getDatai()
    {
        return $this->datai;
    }

    /**
     * Set dataf
     *
     * @param \DateTime $dataf
     *
     * @return Prenotazione
     */
    public function setDataf($dataf)
    {
        $this->dataf = $dataf;

        return $this;
    }

    /**
     * Get dataf
     *
     * @return \DateTime
     */
    public function getDataf()
    {
        return $this->dataf;
    }

    /**
     * Set posto
     *
     * @param string $posto
     *
     * @return Prenotazione
     */
    public function setPosto($posto)
    {
        $this->posto = $posto;

        return $this;
    }

    /**
     * Get posto
     *
     * @return string
     */
    public function getPosto()
    {
        return $this->posto;
    }

    /**
     * Set numpos
     *
     * @param string $numpos
     *
     * @return Prenotazione
     */
    public function setNumpos($numpos)
    {
        $this->numpos = $numpos;

        return $this;
    }

    /**
     * Get numpos
     *
     * @return string
     */
    public function getNumpos()
    {
        return $this->numpos;
    }

    /**
     * Set datains
     *
     * @param \DateTime $datains
     *
     * @return Prenotazione
     */
    public function setDatains($datains)
    {
        $this->datains = $datains;

        return $this;
    }

    /**
     * Get datains
     *
     * @return \DateTime
     */
    public function getDatains()
    {
        return $this->datains;
    }

    /**
     * Set membro
     *
     * @param array $membro
     *
     * @return Prenotazione
     */
    public function setMembro($membro)
    {
        $this->membro = $membro;

        return $this;
    }

    /**
     * Get membro
     *
     * @return array
     */
    public function getMembro()
    {
        return $this->membro;
    }

    /**
     * Set cliente
     *
     * @param \Technomega\AccogliBundle\Entity\Cliente $cliente
     *
     * @return Prenotazione
     */
    public function setCliente(\Technomega\AccogliBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Technomega\AccogliBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}

<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Clienti trovati') ?>

<?php $view['slots']->start('body') ?>
<div class="card">
    <div class="card-header h3">Clienti trovati</div>
    <div class="table-responsive">
        <table class="records_list table table-sm table-striped table-borderless">
            <thead>
            <tr>
                <th>Nominativo</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($clienti as $cliente): ?>
                <tr>
                    <td>
                        <a href="<?php echo $view['router']->path('technomega_accogli_cliente_mostra',
                        array('id' => $cliente->getId())) ?>">
                        <?php echo $cliente->getCognome().' '.$cliente->getNome() ?>
                    </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php $view['slots']->stop() ?>

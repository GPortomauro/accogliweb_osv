<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Lista delle collocazioni') ?>

<?php $view['slots']->start('body') ?>

<div class="card">
    <div class="card-header h3">Lista delle collocazioni <small class="text-muted">(<?php echo $thispage.'/'.$maxpages ?>)</small>
    </div>
    <div class="table-responsive">
        <table class="records_list table table-sm table-striped table-borderless">
            <thead>
            <tr>
                <th>#</th>
                <th>Id Coll.</th>
                <th>Intestatario</th>
                <th>Provenienza</th>
                <th>Prov.</th>
                <th>Data inizio</th>
                <th>Data fine</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1?>
            <?php foreach ($prenotazioni as $prenotazione): ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><a href="<?php echo $view['router']->path('technomega_accogli_prenotazione_mostra',
                            array('id' => $prenotazione->getId())) ?>">
                        <?php echo $prenotazione->getId() ?></td></a>
                    <td><a href="<?php echo $view['router']->path('technomega_accogli_cliente_mostra',
                            array('id' => $prenotazione->getCliente()->getId())) ?>"><?php echo $prenotazione->getCliente() ?></td>
                    <td><?php $nazionalita = $prenotazione->getCliente()->getCitndza();
                        echo $nazionalita == "ITALIA" ? $prenotazione->getCliente()->getCittar() : $prenotazione->getCliente()->getCitndza() ?></td>
                    <td><?php $nazionalita = $prenotazione->getCliente()->getCitndza();
                        echo $nazionalita == "ITALIA" ? $prenotazione->getCliente()->getProvr() : "" ?></td>
                    <td><?php echo $prenotazione->getDatai()->format('d/m/y') ?></td>
                    <td><?php echo $prenotazione->getDataf()->format('d/m/y') ?></td>
                </tr>
                <?php $membri = $prenotazione->getMembro(); if (!empty($membri)):?>
                    <?php foreach ($membri as $membro):?>
                        <?php $i++?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><a href="<?php echo $view['router']->path('technomega_accogli_prenotazione_mostra',
                                    array('id' => $prenotazione->getId())) ?>">
                                <?php echo $prenotazione->getId() ?></td></a>
                            <td><?php echo substr($membro, 5) ?></td>
                            <td><?php $nazionalita = $prenotazione->getCliente()->getCitndza();
                                echo $nazionalita == "ITALIA" ? $prenotazione->getCliente()->getCittar() : $prenotazione->getCliente()->getCitndza() ?></td>
                            <td><?php $nazionalita = $prenotazione->getCliente()->getCitndza();
                                echo $nazionalita == "ITALIA" ? $prenotazione->getCliente()->getProvr() : "" ?></td>
                            <td><?php echo $prenotazione->getDatai()->format('d/m/y') ?></td>
                            <td><?php echo $prenotazione->getDataf()->format('d/m/y') ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif;?>
                <?php $i++?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- Inizio controlli paginatore -->
<?php if ($maxpages > 1) :?>
    <!-- Indietro di n pagine -->
    <?php if ($thispage > 10) :?>
        <a href="<?php echo $view['router']->path('technomega_accogli_prenotazioni', array('page' => $thispage-10)) ?>"><<</a>
    <?php endif; ?>
    <!-- Indietro di 1 pagina -->
    <a href="<?php echo $view['router']->path('technomega_accogli_prenotazioni',
        array('page' => ($thispage-1 < 1 ? 1 : $thispage-1)))?>"><</a>
    <!-- Elenco di n pagine -->
    <?php for ($i = $thispage; $i < $thispage+10 && $i <= $maxpages; $i++) :?>
        <a href="<?php echo $view['router']->path('technomega_accogli_prenotazioni',
            array('page' => $i)) ?>"><?php echo $i ?>
        </a>
    <?php endfor; ?>
    <?php if ($thispage < $maxpages) : ?>
        <!-- Avanti di 1 pagina -->
        <a href="<?php echo $view['router']->path('technomega_accogli_prenotazioni',
            array('page' => ($thispage+1 <= $maxpages ? $thispage+1 : $thispage)))?>">>
        </a>
        <?php if ($thispage < $maxpages-10) : ?>
            <!-- Avanti di n pagine -->
            <?php if ($thispage < $maxpages) :?>
                <a href="<?php echo $view['router']->path('technomega_accogli_prenotazioni', array('page' => $thispage+10)) ?>">>></a>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
<!-- Fine controlli paginatore -->

<?php $view['slots']->stop() ?>

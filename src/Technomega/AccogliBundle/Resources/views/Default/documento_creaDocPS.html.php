<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Creazione Documento per P.S.') ?>
<?php $view['slots']->start('body') ?>
<h3>Creazione Documento per P.S.</h3>

<?php foreach ($view['session']->getFlash('error') as $message): ?>
    <div class="alert-danger">
        <?php echo "<div class='flash-error'>$message</div>" ?>
    </div>
<?php endforeach ?>

<?php /*echo $view['form']->form($form) */?>
<?php echo $view['form']->start($form) ?>
<?php echo $view['form']->row($form['data']) ?>
<p></p>
<div class="row">
    <div class="col-md-4 mb-1">
        <?php echo $view['form']->row($form['Genera doc. telematico'], array('attr' => array('class' => 'btn btn-success')))?>
    </div>
    <div class="col-md-4 mb-1">
        <?php echo $view['form']->row($form['Genera doc. testuale'], array('attr' => array('class' => 'btn btn-success')))?>
    </div>
</div>
<?php echo $view['form']->end($form) ?>

<?php $view['slots']->stop() ?>

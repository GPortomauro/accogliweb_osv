<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Prenotazione effettuata') ?>

<?php $view['slots']->start('body') ?>

<?php $tipoallog = array(
    '0'  => 'Non specificato',
    '16' => 'Ospite Singolo',
    '17' => 'Capofamiglia',
    '18' => 'Capogruppo',
    '19' => 'Familiare',
    '20' => 'Membro Gruppo'); ?>

<?php $tipoposti = array(
    'B' => 'Bungalow',
    'T' => 'Tenda',
    'R' => 'Roulotte',
    'C' => 'Camper',
    'S' => 'Stanziale',
    'MR' => 'MarinaResort'); ?>
<div class="card border-secondary">
    <div class="card-header border-secondary h3">Dettaglio collocazione</div>

    <div class="row" style="padding-left: 1em">
        <div class="col-md-4">
            <span class="badge badge-secondary">Id</span>
            <?php echo $prenotazione->getId() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-8">
            <span class="badge badge-secondary">Cliente</span>
            <?php echo $prenotazione->getCliente() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-4">
            <span class="badge badge-secondary">Data In.</span>
            <?php echo $prenotazione->getDatai()->format('d-m-Y') ?>
        </div>
        <div class="col-md-4">
            <span class="badge badge-secondary">Data Fin.</span>
            <?php echo $prenotazione->getDataf()->format('d-m-Y') ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-4">
            <span class="badge badge-secondary">Tipo</span>
            <?php echo $tipoposti[$prenotazione->getPosto()] ?>
        </div>
        <div class="col-md-4">
            <span class="badge badge-secondary">Posiz./Nr.</span>
            <?php echo $prenotazione->getNumpos() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-4">
            <span class="badge badge-secondary">Tipo allogiato</span>
            <?php echo $tipoallog[$prenotazione->getCodcaf()] ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-12">
            <span class="badge badge-secondary">Membri del gruppo:</span>
            <?php foreach ($prenotazione->getMembro() as $membro): ?>
                <div><?php echo substr($membro, 6) ?></div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-4">
            <span class="badge badge-secondary">Inserita il</span>
            <?php echo $prenotazione->getDatains()->format('d-m-Y') ?>
        </div>
    </div>
</div>

<table class="table table-sm table-borderless d-print-none">
    <tr>
        <td>
            <a class="btn btn-success" href="<?php echo $view['router']->path('technomega_accogli_prenotazione_crea_scheda',
                array('id' => $prenotazione->getId())) ?>" target="_blank">Scheda</a>
        </td>
        <td style="width: 100%; text-align: right">
            <a class="btn btn-warning" href="<?php echo $view['router']->path('technomega_accogli_prenotazione_modifica',
                array('id' => $prenotazione->getId())) ?>">Modifica</a>

            <a class="btn btn-danger" href="<?php echo $view['router']->path('technomega_accogli_prenotazione_cancella',
                array('id' => $prenotazione->getId())) ?>"
               onclick="return confirm('Attenzione! Vuoi veramente eseguire la cancellazione?')">Cancella</a>
        </td>
    </tr>
</table>

<?php $view['slots']->stop() ?>

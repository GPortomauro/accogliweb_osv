<!--
 This file is part of ACCOGLIweb Project.

 Author: Giancarlo Portomauro

 (c) 2014-2015 Technomega
 http://www.technomega.it

 For the full copyright and license information, please view the LICENSE
 file that was distributed with this source code.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Lista delle prenotazioni') ?>

<?php $view['slots']->start('body') ?>
<h3>Lista delle prenotazioni</h3>
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Intestatario</th>
            <th>Provenienza</th>
            <th>Data inizio</th>
            <th>Data fine</th>
        </tr>
     </thead>
     <tbody>
        <?php $i = 0?>
        <?php foreach ($prenotazioni as $prenotazione): ?>
        <tr>
            <td><a href="<?php echo $view['router']->path('technomega_accogli_prenotazione_mostra',
                                                        array('id' => $prenotazione->getId())) ?>">
                <?php echo $prenotazione->getId() ?></td></a>
            <td><?php echo $prenotazione->getCliente() ?></td>
            <td><?php echo $provenienze[$i] ?></td>
            <td><?php echo $prenotazione->getDatai()->format('d-m-Y') ?></td>
            <td><?php echo $prenotazione->getDataf()->format('d-m-Y') ?></td>
        </tr>
            <?php $i++?>
        <?php endforeach; ?>
     </tbody>
</table>

<?php $view['slots']->stop() ?>

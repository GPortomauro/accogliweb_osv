<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Modifica anagrafica cliente') ?>

<?php $view['slots']->start('body') ?>

<?php foreach ($view['session']->getFlash('error') as $message): ?>
    <div class="alert-danger">
        <?php echo "<div class='flash-error'>$message</div>" ?>
    </div>
<?php endforeach ?>

<h3>Modifica anagrafica cliente</h3>
<?php //echo $view['form']->form($form) ?>
<style>
    input[type=text] {
        text-transform: uppercase;
    }
</style>
<?php echo $view['form']->start($form) ?>
<?php echo $view['form']->row($form['cognome']) ?>
<?php echo $view['form']->row($form['nome']) ?>
<?php echo $view['form']->row($form['sex']) ?>
<?php echo $view['form']->row($form['staton']) ?>
<?php echo $view['form']->row($form['cittan']) ?>
<?php echo $view['form']->row($form['provn']) ?>
<?php echo $view['form']->row($form['datan']) ?>
<?php echo $view['form']->row($form['citndza']) ?>
<?php echo $view['form']->row($form['indir']) ?>
<?php echo $view['form']->row($form['cittar']) ?>
<?php echo $view['form']->row($form['provr']) ?>
<?php echo $view['form']->row($form['doctip']) ?>
<?php echo $view['form']->row($form['docnum']) ?>
<?php echo $view['form']->row($form['docdat']) ?>
<?php echo $view['form']->row($form['docloc']) ?>
<?php echo $view['form']->row($form['ets']) ?>
<?php echo $view['form']->row($form['note']) ?>
<?php echo $view['form']->row($form['datins']) ?>
<p></p>
<div class="row">
    <div class="col-md-2">
        <?php echo $view['form']->row($form['Salva'], array('attr' => array('class' => 'btn btn-success'))) ?>
    </div>
    <div class="col-md-2">
        <?php echo $view['form']->row($form['Salva e Prenota'], array('attr' => array('class' => 'btn btn-success'))) ?>
    </div>
</div>
<?php echo $view['form']->end($form) ?>

<?php $view['slots']->stop() ?>

<!--<script>
    var $statonascita = $('#cliente_staton');
    // Quando è stato selezionato lo stato di nascita...
    $statonascita.change(function() {
        // ... recupera il form corrispondente.
        var $form = $(this).closest('form');
        // Simula i dati del form, ma include solo il valore selezionato di sport.
        var data = {};
        data[$statonascita.attr('name')] = $statonascita.val();
        // Invia i dati tramite AJAX al percorso dell'azione del form
        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(html) {
                // Sostituisce il campo della posizione attuale ...
                $('#cliente_cittan').replaceWith(
                    // ... con quello restituito dalla risposta AJAX.
                    $(html).find('#cliente_cittan')
                );
                $('#cliente_docloc').replaceWith(
                    // ... con quello restituito dalla risposta AJAX.
                    $(html).find('#cliente_docloc')
                );
                // Il campo position ora mostra le posizioni appropriate.
            }
        });
    });
</script>-->
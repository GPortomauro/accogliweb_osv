<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Anagrafica del cliente') ?>

<?php $view['slots']->start('body') ?>
<div class="card border-secondary">
    <div class="card-header border-secondary h3">Anagrafica del cliente</div>

<!-- La tipologia del documento è memorizzata nel db come numero, quindi la converto -->
<?php $tipodoc = array(
    'IDENT' => 'Carta d\'Identità',
    'IDELE' => 'Carta d\'Id. Elettr.',
    'PATEN' => 'Patente di Guida',
    'PASOR' => 'Passaporto Ord.'); ?>

    <div class="row" style="padding-left: 1em">
        <div class="col-md-4">
            <span class="badge badge-secondary">Id</span>
            <?php echo $cliente->getId() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-5">
            <span class="badge badge-secondary">Cognome</span>
            <?php echo $cliente->getCognome() ?>
        </div>
        <div class="col-md-5">
            <span class="badge badge-secondary">Nome</span>
            <?php echo $cliente->getNome() ?>
        </div>
        <div class="col-md-2">
            <span class="badge badge-secondary">Sesso</span>
            <?php echo $cliente->getSex() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-4">
            <span class="badge badge-secondary">Data di nascita</span>
            <?php echo $cliente->getDatan()->format('d-m-Y') ?>
        </div>
    </div>
    <span class="badge badge-light">Luogo di nascita</span>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-8">
            <span class="badge badge-secondary">Citt&agrave;</span>
            <?php echo $cliente->getCittan() ?>
        </div>
        <div class="col-md-4">
            <span class="badge badge-secondary">Provincia</span>
            <?php echo $cliente->getProvn() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-8">
            <span class="badge badge-secondary">Stato/Nazione</span>
            <?php echo $cliente->getStaton() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-8">
            <span class="badge badge-secondary">Cittadinanza</span>
            <?php echo $cliente->getCitndza() ?>
        </div>
    </div>
    <span class="badge badge-light">Residenza</span>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-8">
            <span class="badge badge-secondary">Via</span>
            <?php echo $cliente->getIndir() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-md-8">
            <span class="badge badge-secondary">Citt&agrave;</span>
            <?php echo $cliente->getCittar() ?>
        </div>
        <div class="col-md-4">
            <span class="badge badge-secondary">Provincia</span>
            <?php echo $cliente->getProvr() ?>
        </div>
    </div>

    <span class="badge badge-light">Documento d'Identità</span>

    <div class="row" style="padding-left: 1em">
        <div class="col-sm-5">
            <span class="badge badge-secondary">Tipo Documento</span>
            <?php echo $tipodoc[$cliente->getDoctip()] ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-5">
            <span class="badge badge-secondary">Nr.</span>
            <?php echo $cliente->getDocnum() ?>
        </div>
        <div class="col-sm-5">

            <span class="badge badge-secondary">Data rilascio</span>
            <?php echo $cliente->getDocdat()->format('d-m-Y') ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-8">
            <span class="badge badge-secondary">Luogo (Comune o Stato)</span>
            <?php echo $cliente->getDocloc() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-8">
            <span class="badge badge-secondary">Esenzione Tassa Soggiorno</span>
            <?php echo $cliente->getEts() ?>
        </div>
    </div>
    <span class="badge badge-light">Altro</span>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-12">
            <span class="badge badge-secondary">Note</span>
            <?php echo $cliente->getNote() ?>
        </div>
    </div>
    <div class="row" style="padding-left: 1em">
        <div class="col-sm-4">
            <span class="badge badge-secondary">Inserito il</span>
            <?php echo $cliente->getDatins()->format('d-m-Y') ?>
        </div>
    </div>
</div>

<table class="table table-sm table-borderless d-print-none">
    <tr>
        <td>
            <a class="btn btn-light" href="<?php echo $view['router']->path('technomega_accogli') ?>">Home</a>
        </td>
        <td>
            <a class="btn btn-success" href="<?php echo $view['router']->path('technomega_accogli_prenota',
                array('idcliente' => $cliente->getId())) ?>">Colloca</a>
        </td>
        <td style="width: 100%; text-align: right">
            <a class="btn btn-warning" href="<?php echo $view['router']->path('technomega_accogli_cliente_modifica',
                array('id' => $cliente->getId())) ?>">Modifica</a>
            <a class="btn btn-danger" href="<?php echo $view['router']->path('technomega_accogli_cliente_cancella',
                array('id' => $cliente->getId())) ?>"
               onclick="return confirm('Attenzione! Vuoi veramente eseguire la cancellazione?')">Cancella</a>
        </td>
    </tr>
</table>

<?php $view['slots']->stop() ?>

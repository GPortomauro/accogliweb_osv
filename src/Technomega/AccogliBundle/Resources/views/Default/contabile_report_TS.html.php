<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Report Tassa Soggiorno') ?>

<?php $view['slots']->start('body') ?>

<div class="card">
    <div class="card-header h3">Tassa di Soggiorno: Report periodo dal <?php echo $d_dal->format('d/m/Y') ?> al <?php echo $d_al->format('d/m/Y') ?></div>
    <div class="table-responsive">
        <table class="records_list table table-sm table-striped table-borderless">
            <thead>
            <tr>
                <th>#</th>
                <th>Id Coll.</th>
                <th>Cliente</th>
                <th>Età</th>
                <th>Tipo</th>
                <th>Esent. Anagr.</th>
                <th>Provenienza</th>
                <th>Prov.</th>
                <th>Data inizio</th>
                <th>Data fine</th>
                <th>Pernot. imp.</th>
                <th>Importo €</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0; $esa = 0; $stanz = 0; $esm = 0.0; $totnrp = 0; $tot = 0.0?>

            <!--  Array $report_prenotazioni[] = [$id_coll, $nome_membro, $eta, $tipo, $esente, $proven, $provincia,
                                                  $datainiziopren, $datafinepren, $nper, $importo]                 -->
            <?php foreach ($report_prenotazioni as $prenotazione): ?>
                <tr>
                    <td><?php echo $i += 1 ?></td>
                    <td><?php echo $prenotazione[0] ?></td>
                    <td><?php echo $prenotazione[1] ?></td>
                    <td><?php echo $prenotazione[2] ?></td>
                    <?php if ($prenotazione[2] < 15) $esm += 1; ?>
                    <?php if ($prenotazione[3] == 'S'):?>
                        <td style="color:red"><?php echo $prenotazione[3] ?></td>
                    <?php else: ?>
                        <td><?php echo $prenotazione[3] ?></td>
                    <?php endif;?>
                    <?php if ($prenotazione[3] == 'S') $stanz += 1; ?>
                    <td><?php echo $prenotazione[4] ?></td>
                    <?php if ($prenotazione[4] == 'S') $esa += 1; ?>
                    <td><?php echo $prenotazione[5] ?></td>
                    <td><?php echo $prenotazione[6] ?></td>
                    <td><?php echo $prenotazione[7]->format('d/m/y') ?></td>
                    <td><?php echo $prenotazione[8]->format('d/m/y') ?></td>
                    <td><?php echo $prenotazione[9] ?></td>
                    <?php if ($prenotazione[3] != 'S') $totnrp += $prenotazione[9]?>
                    <td><?php if ($prenotazione[3] != 'S') echo number_format($prenotazione[10],'2',',','.') ?></td>
                    <?php if ($prenotazione[3] != 'S') $tot += $prenotazione[10] ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div  class="table table-sm table-striped table-borderless">
        <table>
            <tr>
                <td>Tot. Ospiti/Collocazioni</td><td>&nbsp;</td><td>&nbsp;</td><td><?php echo $i ?></td>
            </tr>
            <tr>
                <td>Stanziali</td><td>&nbsp;</td><td>&nbsp;</td><td style="color:red"><?php echo $stanz ?></td>
            </tr>
            <tr>
                <td>Esenti fino 14 anni</td><td>&nbsp;</td><td>&nbsp;</td><td><?php echo $esm ?></td>
            </tr>
            <tr>
                <td>Altri Esenti</td><td>&nbsp;</td><td>&nbsp;</td><td><?php echo $esa ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            </tr>
            <tr>
                <td>Tot. pernotamenti imponibili</td><td>&nbsp;</td><td>&nbsp;</td><td><?php echo $totnrp ?></td>
                <td>&nbsp;</td><td>Totale Importo €</td><td><?php echo number_format($tot,'2',',','.') ?></td>
            </tr>
        </table>
    </div>
</div>

<?php $view['slots']->stop() ?>

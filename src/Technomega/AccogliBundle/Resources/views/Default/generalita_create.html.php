<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Configurazione delle Generalità') ?>

<?php $view['slots']->start('body') ?>
<h3>Generalità</h3>
<?php //echo $view['form']->form($form) ?>

<?php echo $view['form']->start($form) ?>
<?php echo $view['form']->row($form['ragsoc1']) ?>
<?php echo $view['form']->row($form['tipoattiv']) ?>
<?php echo $view['form']->row($form['attiv']) ?>
<?php echo $view['form']->row($form['idistat']) ?>
<?php echo $view['form']->row($form['indir1']) ?>
<?php echo $view['form']->row($form['cap1']) ?>
<?php echo $view['form']->row($form['citta1']) ?>
<?php echo $view['form']->row($form['prov1']) ?>
<?php echo $view['form']->row($form['indir2']) ?>
<?php echo $view['form']->row($form['cap2']) ?>
<?php echo $view['form']->row($form['citta2']) ?>
<?php echo $view['form']->row($form['prov2']) ?>
<?php echo $view['form']->row($form['piva']) ?>
<?php echo $view['form']->row($form['cofi']) ?>
<?php echo $view['form']->row($form['tel1']) ?>
<?php echo $view['form']->row($form['tel2']) ?>
<?php echo $view['form']->row($form['fax']) ?>
<?php echo $view['form']->row($form['email1']) ?>
<?php echo $view['form']->row($form['email2']) ?>
<?php echo $view['form']->row($form['hpage']) ?>
<?php echo $view['form']->row($form['nrcamere']) ?>
<?php echo $view['form']->row($form['nrletti']) ?>
<?php echo $view['form']->row($form['tassog']) ?>
<p></p>
<?php echo $view['form']->row($form['Salva'], array('attr' => array('class' => 'btn btn-success'))) ?>

<?php echo $view['form']->end($form) ?>

<?php $view['slots']->stop() ?>

<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Ricerca cliente') ?>
<?php $view['slots']->start('body') ?>
<h3>Nominativo del cliente</h3>

<?php foreach ($view['session']->getFlash('notice') as $message): ?>
    <div class="alert-danger">
        <?php echo "<div class='flash-error'>$message</div>" ?>
    </div>
<?php endforeach ?>
<style>
    input[type=text] {
        text-transform: uppercase;
    }
</style>
<p></p>
<?php echo $view['form']->start($form) ?>
<?php echo $view['form']->row($form['cognome'], array('attr' => array('autofocus' => 'autofocus'))) ?>
<?php echo $view['form']->row($form['nome']) ?>
<p></p>
<?php echo $view['form']->row($form['Cerca'], array('attr' => array('class' => 'btn btn-success'))) ?>

<?php echo $view['form']->end($form) ?>

<?php $view['slots']->stop() ?>

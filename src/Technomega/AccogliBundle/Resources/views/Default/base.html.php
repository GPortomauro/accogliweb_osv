<!DOCTYPE html>
<html>
<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php $view['slots']->output('title', 'ACCOGLIweb') ?></title>
	    <?php $view['slots']->output('stylesheets') ?>
        <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
	    <link href="<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/css/main.css') ?>" rel="stylesheet" type="text/css" />
    </head>
    <body>
    <div id="container">
<!--        <img src="<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/images/symfony.gif') ?>" alt="Symfony!" />-->
        <h1> ACCOGLIweb ! </h1>
        <div id="navigation">
            <?php if ($view['slots']->has('sidebar')): ?>
                <?php $view['slots']->output('sidebar') ?>
            <?php else: ?>
                <ul>
                    <li><a href="<?php echo $view['router']->path('technomega_accogli_homepage') ?>">login</a></li>
                </ul>
            <?php endif; ?>
        </div>

        <div id="content">
<!--            <img src="--><?php //echo $view['assets']->getUrl('images/symfony.gif') ?><!--" alt="Symfony!" />-->
            <?php $view['slots']->output('_content') ?>
        </div>
    </div>
    </body>
</html>


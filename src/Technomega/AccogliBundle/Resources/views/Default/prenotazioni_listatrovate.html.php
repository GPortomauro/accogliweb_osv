<!--
 This file is part of ACCOGLIweb Project.

 Author: Giancarlo Portomauro

 (c) 2014-2015 Technomega
 http://www.technomega.it

 For the full copyright and license information, please view the LICENSE
 file that was distributed with this source code.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Lista delle collocazioni') ?>

<?php $view['slots']->start('body') ?>

<div class="card">
    <div class="card-header h3">Lista delle collocazioni</div>
    <div class="table-responsive">
        <table class="records_list table table-sm table-striped table-borderless">
            <thead>
            <tr>
                <th>Id Coll.</th>
                <th>Intestatario</th>
                <th>Provenienza</th>
                <th>Prov.</th>
                <th>Data inizio</th>
                <th>Data fine</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($prenotazioni as $prenotazione): ?>
                <tr>
                    <td><a href="<?php echo $view['router']->path('technomega_accogli_prenotazione_mostra',
                            array('id' => $prenotazione->getId())) ?>">
                        <?php echo $prenotazione->getId() ?></td></a>
                    <td><a href="<?php echo $view['router']->path('technomega_accogli_cliente_mostra',
                            array('id' => $prenotazione->getCliente()->getId())) ?>"><?php echo $prenotazione->getCliente() ?></td>
                    <td><?php $nazionalita = $prenotazione->getCliente()->getCitndza();
                        echo $nazionalita == "ITALIA" ? $prenotazione->getCliente()->getCittar() : $prenotazione->getCliente()->getCitndza() ?>
                    <td><?php $nazionalita = $prenotazione->getCliente()->getCitndza();
                        echo $nazionalita == "ITALIA" ? $prenotazione->getCliente()->getProvr() : "" ?></td>
                    <td><?php echo $prenotazione->getDatai()->format('d/m/y') ?></td>
                    <td><?php echo $prenotazione->getDataf()->format('d/m/y') ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $view['slots']->stop() ?>

<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<?php $view->extend('TechnomegaAccogliBundle:Default:index.html.php') ?>

<?php $view['slots']->set('title', 'Creazione Modello ISTAT C59') ?>
<?php $view['slots']->start('body') ?>
<h3>Creazione Mod. ISTAT C59</h3>

<?php /*echo $view['form']->form($form) */?>
<?php echo $view['form']->start($form) ?>
<?php echo $view['form']->row($form['data']) ?>
<p></p>
<div class="row">
    <div class="col-md-4 mb-1">
<?php echo $view['form']->row($form['Genera file xml'], array('attr' => array('class' => 'btn btn-success')))?>
    </div>
    <div class="col-md-4 mb-1">
<?php echo $view['form']->row($form['Genera report testuale'], array('attr' => array('class' => 'btn btn-success'))) ?>
    </div>
</div>
<?php echo $view['form']->end($form) ?>

<?php $view['slots']->stop() ?>

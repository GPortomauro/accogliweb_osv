<!DOCTYPE html>
<html>
<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $view['slots']->output('title', 'ACCOGLIweb') ?></title>
    <?php $view['slots']->output('stylesheets') ?>
    <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('accogliweb.ico') ?>" />
    <!--    <link href="--><?php //echo $view['assets']->getUrl('bundles/technomegaaccogli/css/main.css') ?><!--" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/css/sticky-footer.css')?>" rel="stylesheet" type="text/css" />

    <style>
        body {
            background-image: url("<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/images/background.gif')?>");
        }

        .my-header-bg-color{
            background-color: #5a6268;
        }

        .my-navbar-bg-color{
            background-color: #cacd75;
        }

        .dropdown-menu{
            background-color: #cacd75;
        }

        .dropdown-item:hover{
            background-color: olive;
        }

        .btn-success{
            background-color: #cacd75;
            border-color: #cacd75;
        }

        .btn-success:hover{
            background-color: olive;
            border-color: olive;
        }
    </style>
</head>

<body>
<header class="blog-header py-1">
    <div class="row my-header-bg-color flex-nowrap justify-content-between align-items-center">
        <div class="col-4 text-center">
            <span class="col-4 blog-header-logo text-light">ACCOGLIweb! </span>
        </div>
        <div class="col-4 text-light text-left">
            <em>Ver.: osv_1.11.1</em>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light my-navbar-bg-color rounded d-print-flex">

        <a class="navbar-brand" href="<?php echo $view['router']->path('technomega_accogli') ?>" style="text-decoration:none;">
            <?php echo $view['actions']->render(
                new \Symfony\Component\HttpKernel\Controller\ControllerReference('TechnomegaAccogliBundle:Default:getSignboard'))?>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars"
                aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbars">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Clienti</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_cliente_inseriscinuovo') ?>">Nuovo</a>
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_clienti') ?>">Elenco</a>
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_cerca') ?>">Cerca</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown02"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Collocazioni</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_prenota') ?>">Nuova</a>
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_prenotazioni_attive') ?>">Presenze</a>
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_prenotazioni_inscadenza') ?>">Partenze</a>
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_prenotazioni') ?>">Tutte</a>
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_prenotazioni_cerca') ?>">Cerca</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown03"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contabile</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_contabile_tassasoggiorno') ?>">Tassa Soggiorno</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Documenti</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03">
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_crea_C59') ?>">Mod. C59</a>
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_crea_docPS') ?>">Dich. P.S.</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown05"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Configurazione</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_configura_configura') ?>">Generalità</a>
                        <?php if ($view['security']->isGranted('ROLE_ADMIN')) :?>
                            <a class="dropdown-item" href="<?php echo $view['router']->path('technomega_accogli_prenotazioni_azzera') ?>"
                               onclick="return confirm('Attenzione! TUTTE le prenotazioni verranno cancellate. Procedo?')">Azzera</a>
                        <?php endif;?>
                    </div>
                </li>
            </ul>
            <span class="text-right">
                    Username: <?php echo $app->getUser()->getUsername() ?>
                <a class="badge badge-danger" href="<?php echo $view['router']->path('logout') ?>"><b>Logout</b></a>
                </span>
        </div>
    </nav>
</header>

<main role="main" class="container">
    <?php foreach ($view['session']->getFlash('error') as $message): ?>
        <div class="alert-danger">
            <?php echo "<div class='flash-error'>$message</div>" ?>
        </div>
    <?php endforeach ?>
    <?php $view['slots']->output('body') ?>
</main>

<footer class="footer">
    <div class="container">
        <span class="text-muted">&copy; 2016<script>new Date().getFullYear()>2010&&document.write("-"+new Date().getFullYear());</script>
            Developed by Technomega</span>
        <!--        <p class="aright">Powered by <a href="http://symfony.com">Symfony</a></p> -->
    </div>
</footer>

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<!--<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>-->
<script src="<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/js/bootstrap.min.js')?>"></script>

</body>
</html>


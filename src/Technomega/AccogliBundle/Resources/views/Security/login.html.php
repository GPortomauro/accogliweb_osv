<!DOCTYPE html>
<html>
<!--
 Copyright (C) 2014-2016 Giancarlo Portomauro

 This file is part of ACCOGLIweb project.

 ACCOGLIweb is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ACCOGLIweb is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ACCOGLIweb. For the full copyright and license information,
 please view the LICENSE file that was distributed with this source code.
 If not, see <http://www.gnu.org/licenses/>.
-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $view['slots']->output('title', 'ACCOGLIweb') ?></title>
    <?php $view['slots']->output('stylesheets') ?>
    <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('accogliweb.ico') ?>" />
    <!--    <link href="--><?php //echo $view['assets']->getUrl('bundles/technomegaaccogli/css/login.css') ?><!--" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $view['assets']->getUrl('bundles/technomegaaccogli/css/signin.css')?>" rel="stylesheet" type="text/css" />
    <style>
        .btn-success{
            background-color: #cacd75;
            border-color: #cacd75;
        }

        .btn-success:hover{
            background-color: olive;
            border-color: olive;
        }
    </style>
</head>
<body>
<div id="content">
    <div id="container">
        <form class="form-signin" role="form" action="<?php echo $view['router']->path('login_check') ?>" method="post">
            <div class="h2 mb-4 font-weight-normal">ACCOGLIweb!</div>
            <?php if ($error): ?>
                <div class="alert-danger"><?php echo $error->getMessage() ?></div>
            <?php endif; ?>
            <div class="h6 mb-3 font-italic">Accesso riservato, inserire le credenziali</div>
            <div>
                <input class="form-control" type="text" id="username" name="_username"
                       value="<?php echo $last_username ?>" placeholder="Username" required autofocus />
            </div>
            <div>
                <input class="form-control" type="password" id="password" name="_password" placeholder="Password" />
            </div>
            <!--
                Se si desidera controllare l'URL a cui l'utente
                viene rinviato in caso di successo (maggiori dettagli qui sotto)
                <input type="hidden" name="_target_path" value="technomega_accogli" />
            -->
            <input type="hidden" name="_csrf_token"
                   value="<?php echo $view['form']->csrfToken('authenticate') ?>">

            <button class="btn btn-lg btn-success btn-block" type="submit">login</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2018 - Developed by Technomega</p>
        </form>
    </div>
</div>
</body>
</html>
